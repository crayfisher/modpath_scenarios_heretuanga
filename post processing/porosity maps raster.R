library(tmap)
library(tmaptools)
library(tidyverse)
library(sf)
library(sp)
library(OpenStreetMap)
library(stringr)

grid <- st_read("gis/grid.shp")
ext1 <- extent(grid)
poros <- raster(matrix(vals,nrow = 302,ncol = 501,byrow = T))
extent(poros) <- ext1

vals <- scan("HPM_IBOUND1.int")
r_i1 <- raster(matrix(vals,nrow = 302,ncol = 501,byrow = T))
extent(r_i1) <- ext1
r_i1[r_i1 == 0] <- NA

vals <- scan("HPM_IBOUND2.int")
r_i2 <- raster(matrix(vals,nrow = 302,ncol = 501,byrow = T))
extent(r_i2) <- ext1
r_i2[r_i2 == 0] <- NA

Sys.sleep(3*3600)
for (layer in (1:2)){
  
  fn1 <- paste("HPM",layer,"._pe",sep="")
  list_par <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest/model_real/",
                  recursive = T,
                  pattern = fn1)
  list_par1 <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest/model_real/",
                   recursive = T,
                   pattern = fn1,
                   full.names = T)
  list_real <- grep("[0-9]+/",list_par,value = T)
  list_real <- as.integer(as.vector(str_match(list_real,"[0-9]+")))
  for (i in 1:length(list_par1)){ #
    if(layer ==1){ri =r_i1}else{ri =r_i2}
    vals <- scan(list_par1[i])
    poros <- raster(matrix(vals,nrow = 302,ncol = 501,byrow = T))
    extent(poros) <- ext1
    poros <- poros * ri
    crs(poros) <- proj4 
    realisation<- list_real[i]
    tm_name <- paste("tm_poros",layer,realisation,sep="_")
    tm_namepng <- paste("tm_poros/",tm_name,".png",sep = "")
    tm_title <- paste("porosity map for \nlayer:",layer,"\nrealisation:",realisation,sep=" ")
    
    tm <- qtm(osm_map3)+
      tm_shape(poros)+
      tm_raster("layer",
                palette = "-Spectral",
                breaks = seq(0,0.18,0.02),
                alpha = 0.7 )+
      tm_scale_bar()+
      tm_compass(position = c("right","top"))
    tm
    tmap_save(tm,tm_namepng)
  } 
}
    