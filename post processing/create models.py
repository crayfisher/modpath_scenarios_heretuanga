# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 17:39:17 2018

@author: pawel.rakowski
"""
import os
import re
import flopy
import shutil




par_location =  "C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/parfiles_fwd_final"
par_location2 =  "C:\PAWEL\MODELLING\HPM\hpm035\modpath scenarios\parfiles_fwd_final"
par_files = os.listdir(par_location)
par = par_files[0]

pst_old = "hpm035_M10_2.pst"
pst_new = "hpm035_M10_new_2.pst"


#this is temporary
os.chdir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest/")


#loop through par files in location, run pest incl modflow
for par in (par_files):
    
    
    #get realisation number
    match = re.search('([0-9]+)\.par',par)
    realisation = match.group(1)
    fld_r = 'model_real/'+realisation+'/'
    
    #check if file exists, if and run only if it doesn't exists, other wise go to next
 
 
    #run parrep 
    par_f = par_location2+"\\"+par
    parrep_cmd = "parrep " + "\'"+par_f +"\'"+" "+pst_old+" "+pst_new   
    
    os.system(parrep_cmd)
    #run pest, incl modflow 
    pest_cmd = "pest " + pst_new 
    os.system(pest_cmd)
    
    #import modflow
    model = "HPM_M3_10.nam"
    mt = flopy.modflow.Modflow.load(model)
    mt.model_ws = fld_r
    mt.write_input()
    shutil.copy2('HPM1._pe', fld_r) 
    shutil.copy2('HPM2._pe', fld_r) 
    scens = ['1','2','3','4']
    
    #this creats files for well scenario
    for sc in (scens):
        file = open("scen.txt", "w") 
        file.write(sc)
        file.close()
        os.system("Rscript create_m3_10_wel.R")
        well_n = ('HPM_M3_10_sc_' + sc + '.wel' )
        dest = fld_r + well_n
        shutil.copy2('HPM_M3_10.wel', dest) 
    


