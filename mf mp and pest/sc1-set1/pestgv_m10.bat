::@echo off
cp obs2.smp obs.smp
del HPM1._kx > nul
del HPM1._kz > nul
del HPM1._ss > nul
del HPM1._sy > nul
del HPM2._kx > nul
del HPM2._kz > nul
del HPM2._ss > nul
del HPM2._sy > nul
del HPM2._pe > nul
del HPM1._pe > nul
del HPM_M3_10.wel > nul
del HPM_M3_10.riv > nul
:: Preprocessing
fac2real < fac2r_kx_L1.in > preprocessing.out   
fac2real < fac2r_kx_L2.in >> preprocessing.out  
fac2real < fac2r_kz_L1.in >> preprocessing.out  
fac2real < fac2r_kz_L2.in >> preprocessing.out  
fac2real < fac2r_Ss_L1.in >> preprocessing.out  
fac2real < fac2r_Ss_L2.in >> preprocessing.out  
fac2real < fac2r_Sy_L1.in >> preprocessing.out  
fac2real < fac2r_Sy_L2.in >> preprocessing.out  
fac2real < fac2r_pe_L1.in >> preprocessing_transp.out  
fac2real < fac2r_pe_L2.in >> preprocessing_transp.out
Rscript create_m3_10_riv.R
Rscript create_m3_10_wel.R
:: Simulation

mf2005 HPM_M3_10.nam > mf2005.out