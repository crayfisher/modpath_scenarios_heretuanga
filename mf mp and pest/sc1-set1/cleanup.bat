del HPM.hds > nul
del HPM_E1.hds > nul
del HPM_E2.hds > nul
del HPM_E3.hds > nul
del HPM_A.hds > nul
del HPM_M1.hds > nul
del HPM_M2.hds > nul
del HPM_80.hds > nul
del head_smp.txt > nul
del hsim_e1.smp > nul
del hsim_e2.smp > nul
del hsim_e3.smp > nul
del hsim_a.smp > nul
del hsim_m1.smp > nul
del hsim_m2.smp > nul
del laydiff_bore_head_sample.txt > nul
del hsim_diff_e1.smp > nul
del hsim_diff_e2.smp > nul
del hsim_diff_e3.smp > nul
del hsim_diff_a.smp > nul
del hsim_diff_m1.smp > nul
del hsim_diff_m2.smp > nul
del drain_smp.txt > nul
del river_smp.txt > nul
del drain_smp.txt > nul

del HPM1._kx > nul
del HPM1._kz > nul
del HPM1._ss > nul
del HPM1._sy > nul
del HPM2._kx > nul
del HPM2._kz > nul
del HPM2._ss > nul
del HPM2._sy > nul


del HPM.wel > nul
del HPM_A.wel > nul
del HPM_M1.wel > nul
del HPM_M2.wel > nul

del preprocessing.out> nul
del wel_gen.out > nul
del mf2005.out > nul
del mf2005_A.out > nul
del mf2005_M1.out > nul
del mf2005_M2.out > nul
del gen_init_heads.out > nul
del postprocessing.out > nul

del AR_diff.smp > nul
del slope_sim.smp > nul
del drain_obs2obs.out > nul
del drain_rec.txt > nul
del river_rec.txt > nul
del laydiff_smp.txt > nul
del *.lst > nul
del *.ref > nul
del *.cbb > nul