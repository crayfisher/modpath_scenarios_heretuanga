# This part of script file added by SVDAPREP
#
# Delete model input files.
/bin/rm point_kx_L1.dat > /dev/null
/bin/rm point_kx_L2.dat > /dev/null
/bin/rm point_kz_L1.dat > /dev/null
/bin/rm point_kz_L2.dat > /dev/null
/bin/rm point_Ss_L1.dat > /dev/null
/bin/rm point_Ss_L2.dat > /dev/null
/bin/rm point_Sy_L1.dat > /dev/null
/bin/rm point_Sy_L2.dat > /dev/null
/bin/rm HPM.riv > /dev/null
/bin/rm HPM_E1.riv > /dev/null
/bin/rm HPM_E2.riv > /dev/null
/bin/rm HPM_E3.riv > /dev/null
/bin/rm HPM_A.riv > /dev/null
/bin/rm HPM_M1.riv > /dev/null
/bin/rm HPM_M2.riv > /dev/null
/bin/rm HPM.ghb > /dev/null
/bin/rm HPM.rch > /dev/null
/bin/rm HPM_80.rch > /dev/null
/bin/rm HPM_A.rch > /dev/null
/bin/rm HPM_M1.rch > /dev/null
/bin/rm HPM_M2.rch > /dev/null
/bin/rm Qpar.dat > /dev/null
/bin/rm HPM.drn > /dev/null
/bin/rm str_mult_f.dat > /dev/null
#
# Run PARCALC to compute base parameters from super parameters.
parcalc > /dev/null
#
# Run PICALC to compute base parameter prior information.
picalc > /dev/null
#
# The following is copied directly from file pestgv
#
#!/bin/sh
set -e

#Misc. cleanup
rm -f HPM.hds
rm -f HPM_E1.hds
rm -f HPM_E2.hds
rm -f HPM_E3.hds
rm -f HPM_A.hds
rm -f HPM_M1.hds
rm -f HPM_M2.hds
rm -f head_smp.txt
rm -f hsim_e1.smp
rm -f hsim_e2.smp
rm -f hsim_e3.smp
rm -f hsim_a.smp
rm -f hsim_m1.smp
rm -f hsim_m2.smp
rm -f laydiff_bore_head_sample.txt
rm -f hsim_diff_e1.smp
rm -f hsim_diff_e2.smp
rm -f hsim_diff_e3.smp
rm -f hsim_diff_a.smp
rm -f hsim_diff_m1.smp
rm -f hsim_diff_m2.smp
rm -f drain_smp.txt
rm -f river_smp.txt
rm -f drain_smp.txt
rm -f slope_sim.smp

rm -f *v2.riv

rm -f HPM1._kx
rm -f HPM1._kz
rm -f HPM1._ss
rm -f HPM1._sy
rm -f HPM2._kx
rm -f HPM2._kz
rm -f HPM2._ss
rm -f HPM2._sy

rm -f head1_for_M1.ref
rm -f head1_for_M2.ref
rm -f head2_for_M1.ref
rm -f head2_for_M2.ref

rm -f HPM_A.wel
rm -f HPM_M1.wel
rm -f HPM_M2.wel

rm -f preprocessing.out> nul
rm -f wel_gen.out > nul
rm -f mf2005.out > nul
rm -f mf2005_A.out > nul
rm -f mf2005_M1.out > nul
rm -f mf2005_M2.out > nul
rm -f gen_init_heads.out > nul
rm -f postprocessing.out > nul

#Preprocessing
fac2real < fac2r_kx_L1.in > preprocessing.out
fac2real < fac2r_kx_L2.in >> preprocessing.out
fac2real < fac2r_kz_L1.in >> preprocessing.out
fac2real < fac2r_kz_L2.in >> preprocessing.out
fac2real < fac2r_Ss_L1.in >> preprocessing.out
fac2real < fac2r_Ss_L2.in >> preprocessing.out
fac2real < fac2r_Sy_L1.in >> preprocessing.out
fac2real < fac2r_Sy_L2.in >> preprocessing.out

Rscript --default-packages=methods create_well_file2.R
Rscript --default-packages=methods riv_mult_proc.R
#Simulation

mf2005 HPM.nam    > mf2005.out
mf2005 HPM_80.nam >> mf2005.out
mf2005 HPM_E1.nam >> mf2005.out
mf2005 HPM_E2.nam >> mf2005.out
mf2005 HPM_E3.nam >> mf2005.out

mf2005 HPM_A.nam > mf2005_A.out
getmularr < getmularr_for_m1m2.in > gen_init_heads.out
mf2005 HPM_M1.nam > mf2005_M1.out
mf2005 HPM_M2.nam > mf2005_M2.out


# Postprocessing
# steady state
mod2obs < head_mod2obs.in > postprocessing.out
mod2obs < laydiff_mod2obs.in >> postprocessing.out
laydiff < laydiff.in >> postprocessing.out
bud2smp < river_bud2smp.in >> postprocessing.out
bud2smp < drain_bud2smp.in >> postprocessing.out
#cp drain_smp.dummy drain_smp.txt

obs2obs drain_obs2obs.in drain_obs2obs.out

# events
mod2obs < head_mod2obs_e1.in >> postprocessing.out
mod2obs < head_mod2obs_e2.in >> postprocessing.out
mod2obs < head_mod2obs_e3.in >> postprocessing.out

smpdiff < smpdiff_e1.in >> postprocessing.out
smpdiff < smpdiff_e2.in >> postprocessing.out
smpdiff < smpdiff_e3.in >> postprocessing.out

# annual and monthly
mod2obs < head_mod2obs_a.in >> postprocessing.out
mod2obs < head_mod2obs_m1.in >> postprocessing.out
mod2obs < head_mod2obs_m2.in >> postprocessing.out

smpdiff < smpdiff_a.in >> postprocessing.out
smpdiff < smpdiff_m1.in >> postprocessing.out
smpdiff < smpdiff_m2.in >> postprocessing.out

Rscript --default-packages=methods read_bud2smp.R

Rscript pest_trend3.R
