# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 17:39:17 2018

@author: pawel.rakowski
"""

import os
import re
import flopy
import feather
import pandas as pd
from flopy.utils import util_array

import numpy as np

par_location =  "../../parfiles_fwd_final"
par_location2 =  "..\..\parfiles_fwd_final"
par_files0 = os.listdir(par_location)
#par_files0 = os.listdir("..")
par_files_subset1 = par_files0[0:53]
par_files_subset2 = par_files0[53:]

pst_old = "hpm035_M10.pst"
pst_new = "hpm035_M10_new.pst"

os.chdir("C:\PAWEL\HBRC\MODELLING\HB\modpath_scenarios_heretuanga\mf mp and pest\sc1-set1")

#check directory name and get scenario and set number
wd = os.getcwd().split('\\')[-1]
scen = re.sub( "sc","",wd.split('-')[0])
set1 = re.sub( "set","",wd.split('-')[1])
scen_n = int(scen)
if set1 == "1":
    par_files =par_files_subset1
elif set1 == "2":
    par_files =par_files_subset2
     
#write scenario no to file
file = open("scen.txt", "w") 
file.write(scen)
file.close()

#import modflow
model = "HPM_M3_10.nam"
mt = flopy.modflow.Modflow.load(model)

mt.get_package_list()
#scenarios data for particles
path = 'my_data.feather'
scen2 = feather.read_dataframe(path)
def fn1(x):
    res= pd.to_numeric(x,downcast = "integer")
    return(res)
def fn2(x):
    res= x-1
    return(res)
scen2 = scen2.apply(fn1)
scen2[["L","C","R"]] = scen2[["L","C","R"]].apply(fn2)
scen2_sc_i = scen2[scen2["scenario"]==scen_n]
scen2_sc_i = scen2_sc_i.drop(columns=["scenario","bore"])
locations = scen2_sc_i.values.tolist()
 



#convert to format for flopy
locations2 = []
loci = 0
for loc in locations:
    locations2.append(locations[loci]+locations[loci])
    loci=loci+1

locations2=[locations2]


#where to place particles on faces
sd = flopy.modpath.FaceDataType(drape=1,
                                    verticaldivisions1=3,
                                    horizontaldivisions1=3,
                                    verticaldivisions2=3,
                                    horizontaldivisions2=3,
                                    verticaldivisions3=3,
                                    horizontaldivisions3=3,
                                    verticaldivisions4=3,
                                    horizontaldivisions4=3,
                                    rowdivisions5=0, #bottom
                                    #columndivisons5=0,
                                    rowdivisions6=3,
                                    columndivisions6=3)
particles = flopy.modpath.LRCParticleData(subdivisiondata = sd,lrcregions=locations2)

releasedata = [119,[0],30.5] #ReleaseTimeCount, InitialReleaseTime, ReleaseInterval

particles_g = flopy.modpath.ParticleGroupLRCTemplate(particledata=particles,releasedata = releasedata)


exe_name="MPath7"
mp_name = "mp1"


mp = flopy.modpath.Modpath7(mp_name, flowmodel=mt,
                            exe_name=exe_name)

mpsim = flopy.modpath.Modpath7Sim(mp, 
                                  simulationtype='timeseries',
                                  trackingdirection='backward',
                                  weaksinkoption='pass_through',
                                  weaksourceoption='pass_through',
                                  budgetoutputoption='summary',
                                  #budgetcellnumbers=[1049, 1259],
                                  #traceparticledata=[1, 1000],
                                  referencetime=[3650.],
                                  stoptimeoption='extend',
                                  timepointdata=[119,30.5], #number of time point, point interval
                                  #zonedataoption='on', zones=zones,
                                  particlegroups=particles_g)

#loop through par files in location, run pest incl modflow
for par in (par_files):
#for par in (['cal_par_w_mt3d_497.par']):
     #par = 'cal_par_w_mt3d_497.par'
    
    #get realisation number
    match = re.search('([0-9]+)\.par',par)
    realisation = match.group(1)
    
    res_name = "mp1_"+scen+"_"+ realisation+".timeseries"
    #check if file exists, if and run only if it doesn't exists, other wise go to next
    exists = os.path.isfile(res_name)
    if exists == False:  
        #run parrep 
        par_f = par_location2+"\\"+par
        parrep_cmd = "parrep " + "\'"+par_f +"\'"+" "+pst_old+" "+pst_new   
        
        os.system(parrep_cmd)
        #run pest, incl modflow and modpath
        pest_cmd = "pest " + pst_new 
        os.system(pest_cmd)

        #get porosit layers
        #load interpolated porosity fields
        pel1=util_array.Util2d.load_txt([mt.nrow,mt.ncol],'HPM1._pe',np.float32,'(free)')
        pel2=util_array.Util2d.load_txt([mt.nrow,mt.ncol],'HPM2._pe',np.float32,'(free)')     
        por=np.zeros((mt.nlay,mt.nrow,mt.ncol)) 
        por[0,:,:]=pel1
        por[1,:,:]=pel2
        mpbas = flopy.modpath.Modpath7Bas(mp, porosity=por)
        # write modpath datasets
        mp.write_input()
        mp.run_model()
        #sve results in new name
        
        mf_lst_name = "hpm035_m10_" +scen+"_"+ realisation+".lst" 
        mp_lst_name = "hpm035_m10_" +scen+"_"+ realisation+".mplst"
        os.system("cp "+ "mp1.timeseries " + res_name)
        os.system("cp "+ "mp1.mplst " + mf_lst_name)
        os.system("cp "+ "HPM_M3.lst " + mp_lst_name)


