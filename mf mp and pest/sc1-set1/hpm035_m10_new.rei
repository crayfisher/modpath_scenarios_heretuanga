
 MODEL OUTPUTS AT END OF OPTIMISATION ITERATION NO.   0:-

 Note that model outputs, measurements and residuals are "rotated" for those 
 observation groups for which a covariance matrix is supplied.

 Note that weights have been adjusted in accordance with regularisation target objective function.


 Name                 Group          Measured         Modelled         Residual         Weight
 obs_one              grp_one        1.000000         1.000000         0.000000         1.000000    
 irch                 regul_rch      1.000000         100.5933        -99.59330        5.0000000E-02
 iq                   regul_q        1.000000         99.43206        -98.43206        5.0000000E-02
 ippkx1               regul_kp1     5.1492054E-02   -0.2134075        0.2648995         Cov. Mat.
 ippkx2               regul_kp1     2.3368068E-03    0.2642890       -0.2619522         Cov. Mat.
 ippkx3               regul_kp1     9.6779337E-03    0.5678147       -0.5581367         Cov. Mat.
 ippkx4               regul_kp1    -0.1819283         1.050127        -1.232055         Cov. Mat.
 ippkx5               regul_kp1    -0.3189698        -1.061919        0.7429492         Cov. Mat.
 ippkx6               regul_kp1     2.6549269E-02   -0.5391326        0.5656819         Cov. Mat.
 ippkx7               regul_kp1    -0.2816967         1.253758        -1.535455         Cov. Mat.
 ippkx8               regul_kp1    -0.2510135       -0.4250306        0.1740171         Cov. Mat.
 ippkx9               regul_kp1    -4.6142488E-02    -1.378604         1.332461         Cov. Mat.
 ippkx10              regul_kp1     0.3995588        0.2737118        0.1258470         Cov. Mat.
 ippkx11              regul_kp1    -3.1156853E-02    0.2033791       -0.2345359         Cov. Mat.
 ippkx12              regul_kp1     0.3938843        0.8938091       -0.4999247         Cov. Mat.
 ippkx13              regul_kp1    -0.6095841        -1.287853        0.6782686         Cov. Mat.
 ippkx14              regul_kp1     0.2540730        0.8182305       -0.5641575         Cov. Mat.
 ippkx15              regul_kp1    -0.2831301       -0.9573544        0.6742243         Cov. Mat.
 ippkx16              regul_kp1     0.1419157         1.011054       -0.8691381         Cov. Mat.
 ippkx17              regul_kp1    -0.3470973       -0.5313260        0.1842287         Cov. Mat.
 ippkx18              regul_kp1     0.1580379        0.2327865       -7.4748657E-02     Cov. Mat.
 ippkx19              regul_kp1    -5.8199658E-02    0.4590294       -0.5172291         Cov. Mat.
 ippkx20              regul_kp1    -0.2641054       -0.2262256       -3.7879762E-02     Cov. Mat.
 ippkx21              regul_kp1     8.6027666E-02   -0.9598185         1.045846         Cov. Mat.
 ippkx22              regul_kp1     4.3804090E-02    -1.017259         1.061063         Cov. Mat.
 ippkx23              regul_kp1    -0.2550781       -0.3264542        7.1376127E-02     Cov. Mat.
 ippkx24              regul_kp1     0.1041574        0.5964763       -0.4923190         Cov. Mat.
 ippkx25              regul_kp1     4.9631508E-02    0.5233447       -0.4737132         Cov. Mat.
 ippkx26              regul_kp1    -0.3297102        0.2696634       -0.5993736         Cov. Mat.
 ippkx27              regul_kp1     0.1722359       -0.1210120        0.2932480         Cov. Mat.
 ippkx28              regul_kp1    -9.9476790E-02    -1.131059         1.031582         Cov. Mat.
 ippkx29              regul_kp1    -0.3295957       -0.2622351       -6.7360663E-02     Cov. Mat.
 ippkx30              regul_kp1    -0.3400889       -0.4426476        0.1025587         Cov. Mat.
 ippkx31              regul_kp1     5.2644411E-03    0.7381186       -0.7328541         Cov. Mat.
 ippkx32              regul_kp1     2.1063525E-02   -0.2358452        0.2569087         Cov. Mat.
 ippkx33              regul_kp1     0.2184284        -1.485581         1.704009         Cov. Mat.
 ippkx34              regul_kp1    -0.1459632        0.4207861       -0.5667493         Cov. Mat.
 ippkx35              regul_kp1    -0.3099358       -0.4906498        0.1807140         Cov. Mat.
 ippkx36              regul_kp1    -0.6023864       -0.6503239        4.7937512E-02     Cov. Mat.
 ippkx37              regul_kp1     3.2676950E-02    0.9099592       -0.8772823         Cov. Mat.
 ippkx38              regul_kp1     0.1178996       -0.1122596        0.2301593         Cov. Mat.
 ippkx39              regul_kp1    -0.1782623       -3.1873467E-02   -0.1463889         Cov. Mat.
 ippkx40              regul_kp1    -0.4294271       -0.8948772        0.4654501         Cov. Mat.
 ippkx41              regul_kp1    -0.5300987       -0.6171399        8.7041210E-02     Cov. Mat.
 ippkx42              regul_kp1     0.3948826         1.442024        -1.047142         Cov. Mat.
 ippkx43              regul_kp1     5.7373012E-02    0.4341500       -0.3767770         Cov. Mat.
 ippkx44              regul_kp1     0.2715741       -0.1750472        0.4466213         Cov. Mat.
 ippkx45              regul_kp1     0.2288446       -0.2731301        0.5019747         Cov. Mat.
 ippkx46              regul_kp1    -0.1727880       -0.9120265        0.7392385         Cov. Mat.
 ippkx47              regul_kp1    -7.5637844E-02   -0.3216053        0.2459674         Cov. Mat.
 ippkx48              regul_kp1     6.3695727E-02     1.201546        -1.137850         Cov. Mat.
 ippkx49              regul_kp1    -0.1941551       -0.5186527        0.3244976         Cov. Mat.
 ippkx50              regul_kp1    -7.6017723E-02   -0.3091808        0.2331630         Cov. Mat.
 ippkx51              regul_kp1     0.2655525       -0.5759777        0.8415301         Cov. Mat.
 ippkx52              regul_kp1     0.3748728        0.6682351       -0.2933624         Cov. Mat.
 ippkx53              regul_kp1    -0.2608563         1.561759        -1.822615         Cov. Mat.
 ippkx54              regul_kp1     0.1670759        0.6873594       -0.5202835         Cov. Mat.
 ippkx55              regul_kp1     0.1367756        0.3952009       -0.2584252         Cov. Mat.
 ippkx56              regul_kp1    -0.5027724       -0.3397388       -0.1630336         Cov. Mat.
 ippkx57              regul_kp1     2.3679639E-02    0.1780892       -0.1544096         Cov. Mat.
 ippkx58              regul_kp1    -0.8820771       -0.3652642       -0.5168129         Cov. Mat.
 ippkx59              regul_kp1     0.1877251         1.353685        -1.165960         Cov. Mat.
 ippkx60              regul_kp1    -8.5730037E-02    0.7746217       -0.8603518         Cov. Mat.
 ippkx61              regul_kp1    -0.2752800       -0.9490027        0.6737227         Cov. Mat.
 ippkx62              regul_kp1     7.6837498E-02     1.318522        -1.241685         Cov. Mat.
 ippkx63              regul_kp1     0.4812990        0.5071191       -2.5820177E-02     Cov. Mat.
 ippkx64              regul_kp1    -9.7022644E-02    0.3074584       -0.4044810         Cov. Mat.
 ippkx65              regul_kp1    -0.2333272       -0.3758482        0.1425209         Cov. Mat.
 ippkx66              regul_kp1     0.3613582        0.9514737       -0.5901155         Cov. Mat.
 ippkx67              regul_kp1    -0.5562437       -4.9783471E-02   -0.5064602         Cov. Mat.
 ippkx68              regul_kp1    -0.1747216        0.2890291       -0.4637507         Cov. Mat.
 ippkx69              regul_kp1    -0.1958698       -0.2495692        5.3699360E-02     Cov. Mat.
 ippkx70              regul_kp1    -0.3844344        0.3199389       -0.7043733         Cov. Mat.
 ippkx71              regul_kp1     0.2392004        0.8050130       -0.5658125         Cov. Mat.
 ippkx72              regul_kp1     0.4229344        0.5531245       -0.1301900         Cov. Mat.
 ippkx73              regul_kp1    -0.1287318        -1.302967         1.174236         Cov. Mat.
 ippkx74              regul_kp1     0.4005271        -1.362600         1.763127         Cov. Mat.
 ippkx75              regul_kp1     0.1593895        -1.206088         1.365477         Cov. Mat.
 ippkx76              regul_kp1    -0.2160037       -0.1466871       -6.9316588E-02     Cov. Mat.
 ippkx77              regul_kp1    -0.3919655       -0.6806872        0.2887216         Cov. Mat.
 ippkx78              regul_kp1    -0.1409541         1.490394        -1.631348         Cov. Mat.
 ippkx79              regul_kp1     9.4285339E-02   -0.7495854        0.8438708         Cov. Mat.
 ippkx80              regul_kp1     0.1236488        0.1046475        1.9001325E-02     Cov. Mat.
 ippkx81              regul_kp1     0.1772471        0.8319514       -0.6547043         Cov. Mat.
 ippkx82              regul_kp1    -0.4842224        0.8251895        -1.309412         Cov. Mat.
 ippkx83              regul_kp1     0.5451068       -6.7888088E-02    0.6129949         Cov. Mat.
 ippkx84              regul_kp1     0.1920811        0.3933961       -0.2013150         Cov. Mat.
 ippkx85              regul_kp1     0.1047020         1.786339        -1.681637         Cov. Mat.
 ippkx86              regul_kp1    -0.3134647        0.3084339       -0.6218986         Cov. Mat.
 ippkx87              regul_kp1     0.3657670        -1.278218         1.643985         Cov. Mat.
 ippkx88              regul_kp1     0.4009162        0.6005430       -0.1996268         Cov. Mat.
 ippkx89              regul_kp1     6.1950295E-02     2.270266        -2.208316         Cov. Mat.
 ippkx90              regul_kp1     0.8250901        8.5965506E-02    0.7391246         Cov. Mat.
 ippkx91              regul_kp1    -0.7668524        -1.278573        0.5117207         Cov. Mat.
 ippkx92              regul_kp1     0.6437967         1.212793       -0.5689963         Cov. Mat.
 ippkx93              regul_kp1    -0.4538677         1.151838        -1.605706         Cov. Mat.
 ippkx94              regul_kp1     1.6749301E-02    0.1004075       -8.3658164E-02     Cov. Mat.
 ippkx95              regul_kp1     0.2702531        0.2981821       -2.7928994E-02     Cov. Mat.
 ippkx96              regul_kp1    -3.2921978E-02   -0.7966220        0.7637001         Cov. Mat.
 ippkx97              regul_kp1    -2.6958953E-02    0.1055905       -0.1325494         Cov. Mat.
 ippkx98              regul_kp1     0.2631871        6.3563773E-02    0.1996233         Cov. Mat.
 ippkx99              regul_kp2    -3.4061316E-03    0.1158255       -0.1192316         Cov. Mat.
 ippkx100             regul_kp2    -7.4054881E-03    0.5706463       -0.5780518         Cov. Mat.
 ippkx101             regul_kp2    -6.3861472E-03    0.8991097       -0.9054959         Cov. Mat.
 ippkx102             regul_kp2     4.9043639E-03    -1.014903         1.019807         Cov. Mat.
 ippkx103             regul_kp2     3.7447302E-03    6.5819558E-02   -6.2074828E-02     Cov. Mat.
 ippkx104             regul_kp2    -2.1578753E-03   -0.4895090        0.4873512         Cov. Mat.
 ippkx105             regul_kp2    -4.4206010E-03    0.6331032       -0.6375238         Cov. Mat.
 ippkx106             regul_kp2     1.7349645E-03   -0.2870528        0.2887877         Cov. Mat.
 ippkx107             regul_kp2    -4.6216707E-03    0.4588941       -0.4635158         Cov. Mat.
 ippkx108             regul_kp2    -1.4036432E-02   -0.1626781        0.1486416         Cov. Mat.
 ippkx109             regul_kp2    -7.6691821E-04    0.5050926       -0.5058595         Cov. Mat.
 ippkx110             regul_kp2     1.9678256E-02    -1.063436         1.083115         Cov. Mat.
 ippkx111             regul_kp2     4.5797967E-04    3.0766319E-02   -3.0308339E-02     Cov. Mat.
 ippkx112             regul_kp2     7.1610442E-04     1.220580        -1.219864         Cov. Mat.
 ippkx113             regul_kp2     2.7521309E-03    7.5451124E-02   -7.2698993E-02     Cov. Mat.
 ippkx114             regul_kp2    -2.1686852E-03   -0.5548711        0.5527025         Cov. Mat.
 ippkx115             regul_kp2    -8.0323019E-04   -0.5323039        0.5315007         Cov. Mat.
 ippkx116             regul_kp2     4.1601477E-02    0.4640817       -0.4224802         Cov. Mat.
 ippkx117             regul_kp2     2.3742660E-02   -0.7943404        0.8180830         Cov. Mat.
 ippkx118             regul_kp2     7.3764414E-03    -1.211920         1.219297         Cov. Mat.
 ippkx119             regul_kp2    -6.0698959E-03   -0.1693222        0.1632523         Cov. Mat.
 ippkx120             regul_kp2    -1.5735787E-02   -0.2879813        0.2722455         Cov. Mat.
 ippkx121             regul_kp2    -3.0182503E-03   -0.6068622        0.6038439         Cov. Mat.
 ippkx122             regul_kp2     2.2642257E-02   -0.2649785        0.2876207         Cov. Mat.
 ippkx123             regul_kp2    -4.6610789E-03    0.8908472       -0.8955082         Cov. Mat.
 ippkx124             regul_kp2    -2.8233735E-03    0.1185343       -0.1213577         Cov. Mat.
 ippkx125             regul_kp2    -1.4926604E-02   -0.5408545        0.5259279         Cov. Mat.
 ippkx126             regul_kp2     5.2282041E-03   -6.2587677E-02    6.7815881E-02     Cov. Mat.
 ippkx127             regul_kp2    -5.8967079E-03     1.043875        -1.049772         Cov. Mat.
 ippkx128             regul_kp2     2.7779195E-03   -0.5857300        0.5885079         Cov. Mat.
 ippkx129             regul_kp2    -3.3064007E-03    0.2157391       -0.2190455         Cov. Mat.
 ippkx130             regul_kp2    -7.0342323E-03   -0.3197083        0.3126740         Cov. Mat.
 ippkx131             regul_kp2     1.2187860E-02    4.6336124E-03    7.5542478E-03     Cov. Mat.
 ippkx132             regul_kp2    -6.5839117E-03    -1.021128         1.014544         Cov. Mat.
 ippkx133             regul_kp2     1.9235839E-02   -0.7688736        0.7881095         Cov. Mat.
 ippkx134             regul_kp2     1.7258988E-02    -1.542269         1.559528         Cov. Mat.
 ippkx135             regul_kp2     3.1923907E-02    1.2784015E-02    1.9139893E-02     Cov. Mat.
 ippkx136             regul_kp2    -3.1060063E-03   -0.1761470        0.1730410         Cov. Mat.
 ippkx137             regul_kp2    -2.6854792E-02    8.0782070E-02   -0.1076369         Cov. Mat.
 ippkx138             regul_kp2    -3.7544626E-02    0.7948055       -0.8323501         Cov. Mat.
 ippkx139             regul_kp2     5.5163852E-02     1.141438        -1.086274         Cov. Mat.
 ippkx140             regul_kp2     5.8471778E-02    -1.193067         1.251539         Cov. Mat.
 ippkx141             regul_kp2    -5.8721140E-03     1.233336        -1.239208         Cov. Mat.
 ippkx142             regul_kp2     2.0136630E-02    4.2034899E-02   -2.1898270E-02     Cov. Mat.
 ippkx143             regul_kp2    -2.8651722E-03    0.2164593       -0.2193245         Cov. Mat.
 ippkx144             regul_kp2    -2.7384742E-02    0.2976228       -0.3250076         Cov. Mat.
 ippkx145             regul_kp2    -2.7710065E-02    -2.003113         1.975403         Cov. Mat.
 ippkx146             regul_kp2     1.5397988E-02   -0.2808526        0.2962506         Cov. Mat.
 ippkx147             regul_kp2     4.1265673E-03     1.185352        -1.181225         Cov. Mat.
 ippkx148             regul_kp2     1.5614612E-03    -1.545546         1.547107         Cov. Mat.
 ippkx149             regul_kp2     3.9523484E-03   -0.8781911        0.8821435         Cov. Mat.
 ippkx150             regul_kp2    -1.2770104E-02     1.498670        -1.511440         Cov. Mat.
 ippkx151             regul_kp2    -4.9685507E-02    -1.489782         1.440096         Cov. Mat.
 ippkx152             regul_kp2    -1.1370836E-02   -2.9326157E-02    1.7955321E-02     Cov. Mat.
 ippkx153             regul_kp2    -4.9907259E-02    0.6261420       -0.6760492         Cov. Mat.
 ippkx154             regul_kp2     4.8513515E-02    0.8273592       -0.7788457         Cov. Mat.
 ippkx155             regul_kp2     6.0620626E-02   -0.3311161        0.3917368         Cov. Mat.
 ippkx156             regul_kp2     0.1213979       -0.9704920         1.091890         Cov. Mat.
 ippkx157             regul_kp2     7.9898739E-02    0.8637205       -0.7838217         Cov. Mat.
 ippkx158             regul_kp2     0.1024938       -0.3293964        0.4318903         Cov. Mat.
 ippkx159             regul_kp2    -6.1847479E-02    -1.857162         1.795315         Cov. Mat.
 ippkx160             regul_kp2     9.7120708E-02   -0.6316065        0.7287272         Cov. Mat.
 ippkx161             regul_kp2     4.9663321E-02   -0.3006842        0.3503475         Cov. Mat.
 ippkx162             regul_kp2     4.4016555E-02   -0.1811039        0.2251204         Cov. Mat.
 ippkx163             regul_kp2     2.4070144E-02     1.549048        -1.524978         Cov. Mat.
 ippkx164             regul_kp2     0.1076495        0.1932775       -8.5628037E-02     Cov. Mat.
 ippkx165             regul_kp2     9.1267924E-02    0.1124906       -2.1222630E-02     Cov. Mat.
 ippkx166             regul_kp2    -5.8692951E-02    0.6025069       -0.6611998         Cov. Mat.
 ippkx167             regul_kp2    -3.3354123E-02    -1.542187         1.508833         Cov. Mat.
 ippkx168             regul_kp2    -0.1890043         1.115816        -1.304820         Cov. Mat.
 ippkx169             regul_kp2     0.1957860         1.269859        -1.074073         Cov. Mat.
 ippkx170             regul_kp2     0.1485204        -1.573347         1.721867         Cov. Mat.
 ippkx171             regul_kp2    -2.7329342E-02   -0.8371996        0.8098702         Cov. Mat.
 ippkx172             regul_kp2    -0.1426970       -0.2542074        0.1115104         Cov. Mat.
 ippkx173             regul_kp2    -0.1282736       -0.4081135        0.2798398         Cov. Mat.
 ippkx174             regul_kp2     7.7230679E-02   -0.7001977        0.7774284         Cov. Mat.
 ippkx175             regul_kp2    -0.2606251        -1.718104         1.457479         Cov. Mat.
 ippkx176             regul_kp2     0.3177166        -3.465201         3.782918         Cov. Mat.
 ippkx177             regul_kp2    -0.3700133        -1.308529        0.9385161         Cov. Mat.
 ippkx178             regul_kp2    -0.6655585        -2.705065         2.039506         Cov. Mat.
 ippkx179             regul_kp2    -0.2940150        2.1825842E-02   -0.3158408         Cov. Mat.
 ippkx180             regul_kp2    -1.5119315E-02    -2.232276         2.217157         Cov. Mat.
 ippkx181             regul_kp2     0.2809757        -1.217306         1.498281         Cov. Mat.
 ippkx182             regul_kp2     9.1147971E-02    -1.778361         1.869509         Cov. Mat.
 ippkx183             regul_kp2     0.1722441        0.6828739       -0.5106298         Cov. Mat.
 ippkx184             regul_kp1    -6.9125031E-02   -0.7400063        0.6708813         Cov. Mat.
 ippkx185             regul_kp1     0.6191294        1.0256505E-02    0.6088729         Cov. Mat.
 ippkx186             regul_kp1    -0.3162719       -0.5089861        0.1927143         Cov. Mat.
 ippkx187             regul_kp1     0.4961879        0.9907795       -0.4945916         Cov. Mat.
 ippkx188             regul_kp1     5.7859065E-02   -0.4645083        0.5223673         Cov. Mat.
 ippkx189             regul_kp1     0.2376076        0.2272935        1.0314084E-02     Cov. Mat.
 ippkx190             regul_kp1    -0.9561885        -1.393105        0.4369167         Cov. Mat.
 ippkx191             regul_kp1    -0.1082414       -0.5333366        0.4250951         Cov. Mat.
 ippkx192             regul_kp1    -8.8995618E-02    -1.056041        0.9670454         Cov. Mat.
 ippkx193             regul_kp1    -0.1521654       -0.6901750        0.5380096         Cov. Mat.
 ippkx194             regul_kp1     0.5817197       -0.7851684         1.366888         Cov. Mat.
 ippkx195             regul_kp1    -0.1568993        0.3703667       -0.5272661         Cov. Mat.
 ippkx196             regul_kp1     0.7894543         1.926692        -1.137237         Cov. Mat.
 ippkx197             regul_kp1     0.2996941       -0.2496996        0.5493937         Cov. Mat.
 ippkx198             regul_kp1    -0.6280151        9.8306896E-02   -0.7263220         Cov. Mat.
 ippkx199             regul_kp1    -4.1503850E-02   -0.4384234        0.3969196         Cov. Mat.
 ippkx200             regul_kp1    -0.4327599        0.6999627        -1.132723         Cov. Mat.
 ippkx201             regul_kp1    -0.2565772         2.083798        -2.340375         Cov. Mat.
 ippkx202             regul_kp1    -0.2215898         1.108535        -1.330125         Cov. Mat.
 ippkx203             regul_kp1     9.8307558E-02    -3.144940         3.243247         Cov. Mat.
 ippkx204             regul_kp1    -0.4836347        -1.955164         1.471529         Cov. Mat.
 ippkx205             regul_kp1    -0.1671202       -0.5418164        0.3746962         Cov. Mat.
 ippkx206             regul_kp1    -0.1956965       -0.7942510        0.5985545         Cov. Mat.
 ippkx207             regul_kp1    -0.6359401       -0.2323638       -0.4035763         Cov. Mat.
 ippkx208             regul_kp1      2.000000         1.229805        0.7701946         Cov. Mat.
 ippkx209             regul_kp1     0.9416473        0.5046593        0.4369880         Cov. Mat.
 ippkx210             regul_kp1      1.000000         1.141142       -0.1411420         Cov. Mat.
 ippkx211             regul_kp1    -0.3366013       -0.9494760        0.6128747         Cov. Mat.
 ippkx212             regul_kp1    -0.7603009        -1.140734        0.3804335         Cov. Mat.
 ippkx213             regul_kp1     0.3012080        0.6408761       -0.3396681         Cov. Mat.
 ippkx214             regul_kp1     0.1033792       -0.5809330        0.6843122         Cov. Mat.
 ippkx215             regul_kp1     -1.774912        0.8402667        -2.615178         Cov. Mat.
 ippkx216             regul_kp1     9.2484652E-02   -0.9999880         1.092473         Cov. Mat.
 ippkx217             regul_kp1     0.9963256         2.720597        -1.724272         Cov. Mat.
 ippkx218             regul_kp1     -1.102635        -1.010589       -9.2045656E-02     Cov. Mat.
 ippkx219             regul_kp1     0.2038160        0.5283813       -0.3245653         Cov. Mat.
 ippkx220             regul_kp1     -1.731561        -1.976642        0.2450810         Cov. Mat.
 ippkx221             regul_kp1    -0.6251997        -1.573695        0.9484948         Cov. Mat.
 ippkx222             regul_kp1    -0.5979077        0.1327782       -0.7306860         Cov. Mat.
 ippkx223             regul_kp1    -0.5290795        -2.606960         2.077880         Cov. Mat.
 ippkx224             regul_kp1    -0.2957332         1.644965        -1.940699         Cov. Mat.
 ippkx225             regul_kp1     -4.124126        -3.257681       -0.8664453         Cov. Mat.
 ippkx226             regul_kp1      1.599279         2.685772        -1.086493         Cov. Mat.
 ippkx227             regul_kp1     -2.240930        -2.159393       -8.1536555E-02     Cov. Mat.
 ippkx228             regul_kp1      1.514366       -0.3404171         1.854783         Cov. Mat.
 ippkx229             regul_kp2      2.242788        0.4918186         1.750970         Cov. Mat.
 ippkx230             regul_kp2      1.674773         1.332946        0.3418271         Cov. Mat.
 ippkx231             regul_kp2      2.069454       -0.8049903         2.874444         Cov. Mat.
 ippkx232             regul_kp2     0.3767325       -0.8000679         1.176800         Cov. Mat.
 ippkx233             regul_kp2     0.5146311        -1.631073         2.145704         Cov. Mat.
 ippkx234             regul_kp1     -29.66692        -21.47997        -8.186948         Cov. Mat.
 ippkx235             regul_kp2     -25.48864        -17.97395        -7.514696         Cov. Mat.
 ippkz1               regul_kz1     1.2889339E-03    4.6743245E-02   -4.5454311E-02     Cov. Mat.
 ippkz2               regul_kz1     3.0988313E-03    0.4639631       -0.4608643         Cov. Mat.
 ippkz3               regul_kz1    -4.5640510E-04   -0.6625804        0.6621240         Cov. Mat.
 ippkz4               regul_kz1    -2.3366689E-03   -0.1359536        0.1336170         Cov. Mat.
 ippkz5               regul_kz1    -1.7887301E-04    0.4174648       -0.4176437         Cov. Mat.
 ippkz6               regul_kz1    -1.4861397E-04   -0.7994364        0.7992878         Cov. Mat.
 ippkz7               regul_kz1    -1.5278678E-03    0.7882465       -0.7897744         Cov. Mat.
 ippkz8               regul_kz1    -6.5752709E-03   -0.9579072        0.9513319         Cov. Mat.
 ippkz9               regul_kz1     2.5829496E-05    0.6464948       -0.6464690         Cov. Mat.
 ippkz10              regul_kz1     3.6953811E-03   -0.1996297        0.2033250         Cov. Mat.
 ippkz11              regul_kz1    -3.7729116E-03   -0.3223707        0.3185978         Cov. Mat.
 ippkz12              regul_kz1     6.4295770E-03    0.5125767       -0.5061471         Cov. Mat.
 ippkz13              regul_kz1     3.6631487E-03    4.1210990E-03   -4.5795035E-04     Cov. Mat.
 ippkz14              regul_kz1    -6.0839249E-05   -0.7001564        0.7000955         Cov. Mat.
 ippkz15              regul_kz1    -9.6484200E-04   -0.9768460        0.9758811         Cov. Mat.
 ippkz16              regul_kz1     3.0841965E-03   -5.3652017E-02    5.6736214E-02     Cov. Mat.
 ippkz17              regul_kz1     6.4942947E-03   -2.7827757E-02    3.4322052E-02     Cov. Mat.
 ippkz18              regul_kz1    -4.8861268E-03   -0.3836364        0.3787503         Cov. Mat.
 ippkz19              regul_kz1    -5.0707182E-03   -0.2695215        0.2644507         Cov. Mat.
 ippkz20              regul_kz1     1.8499471E-03    0.7356912       -0.7338412         Cov. Mat.
 ippkz21              regul_kz1    -4.2391140E-03   -0.3492404        0.3450013         Cov. Mat.
 ippkz22              regul_kz1     2.1075439E-03    0.3829255       -0.3808179         Cov. Mat.
 ippkz23              regul_kz1    -2.2615525E-03   -0.6985482        0.6962866         Cov. Mat.
 ippkz24              regul_kz1     2.8170658E-03   -5.0403815E-02    5.3220881E-02     Cov. Mat.
 ippkz25              regul_kz1    -5.9687743E-03    0.2376121       -0.2435809         Cov. Mat.
 ippkz26              regul_kz1     1.0755470E-03   -0.1015172        0.1025928         Cov. Mat.
 ippkz27              regul_kz1     7.9658042E-03   -9.0103511E-02    9.8069315E-02     Cov. Mat.
 ippkz28              regul_kz1     5.1813953E-03    0.3130267       -0.3078453         Cov. Mat.
 ippkz29              regul_kz1     4.4187728E-03    0.1000780       -9.5659179E-02     Cov. Mat.
 ippkz30              regul_kz1    -3.7016883E-03    0.2285397       -0.2322414         Cov. Mat.
 ippkz31              regul_kz1    -1.5448604E-03   -0.1891979        0.1876530         Cov. Mat.
 ippkz32              regul_kz1     9.5541451E-03   -6.1641007E-02    7.1195152E-02     Cov. Mat.
 ippkz33              regul_kz1     7.7152653E-03    0.9733420       -0.9656268         Cov. Mat.
 ippkz34              regul_kz1    -2.5631851E-03   -0.2542004        0.2516372         Cov. Mat.
 ippkz35              regul_kz1    -1.0074508E-02    0.3224509       -0.3325254         Cov. Mat.
 ippkz36              regul_kz1    -5.1371996E-03    7.5619021E-02   -8.0756220E-02     Cov. Mat.
 ippkz37              regul_kz1     2.2103885E-02    0.3087232       -0.2866193         Cov. Mat.
 ippkz38              regul_kz1     3.8468222E-04    0.6822914       -0.6819068         Cov. Mat.
 ippkz39              regul_kz1     1.9450207E-03    0.5861155       -0.5841705         Cov. Mat.
 ippkz40              regul_kz1     9.4450847E-04    0.8280363       -0.8270918         Cov. Mat.
 ippkz41              regul_kz1    -3.4789887E-03   -0.2327091        0.2292301         Cov. Mat.
 ippkz42              regul_kz1     1.5652986E-02    0.9984314       -0.9827785         Cov. Mat.
 ippkz43              regul_kz1    -1.9838528E-03    0.2502041       -0.2521880         Cov. Mat.
 ippkz44              regul_kz1     1.5743575E-02     1.088084        -1.072340         Cov. Mat.
 ippkz45              regul_kz1     1.1149569E-02    0.5530021       -0.5418526         Cov. Mat.
 ippkz46              regul_kz1     3.6318000E-03    0.3873882       -0.3837564         Cov. Mat.
 ippkz47              regul_kz1     7.6661096E-03   -0.3917903        0.3994564         Cov. Mat.
 ippkz48              regul_kz1     1.2006318E-03    0.1207776       -0.1195769         Cov. Mat.
 ippkz49              regul_kz1    -1.5138986E-02    0.7083313       -0.7234703         Cov. Mat.
 ippkz50              regul_kz1    -1.9268904E-03    0.7594239       -0.7613507         Cov. Mat.
 ippkz51              regul_kz1     1.0321680E-02    9.8655736E-02   -8.8334056E-02     Cov. Mat.
 ippkz52              regul_kz1    -1.7860515E-03    0.2365465       -0.2383326         Cov. Mat.
 ippkz53              regul_kz1     4.4421146E-03    8.7173463E-02   -8.2731349E-02     Cov. Mat.
 ippkz54              regul_kz1     9.2512629E-03    4.2556099E-02   -3.3304836E-02     Cov. Mat.
 ippkz55              regul_kz1     1.0929891E-02    6.2053176E-02   -5.1123284E-02     Cov. Mat.
 ippkz56              regul_kz1     8.6443606E-03    0.2819460       -0.2733016         Cov. Mat.
 ippkz57              regul_kz1     2.8370910E-02     1.148023        -1.119652         Cov. Mat.
 ippkz58              regul_kz1    -1.6101347E-02    2.7190546E-02   -4.3291892E-02     Cov. Mat.
 ippkz59              regul_kz1     1.2665466E-02    0.8193132       -0.8066477         Cov. Mat.
 ippkz60              regul_kz1     1.1146171E-02     1.226941        -1.215794         Cov. Mat.
 ippkz61              regul_kz1    -3.9545103E-02    0.4242961       -0.4638412         Cov. Mat.
 ippkz62              regul_kz1     2.6379266E-02    0.2709315       -0.2445522         Cov. Mat.
 ippkz63              regul_kz1    -7.9794029E-04    0.9497942       -0.9505921         Cov. Mat.
 ippkz64              regul_kz1    -1.2009439E-02    0.3468004       -0.3588099         Cov. Mat.
 ippkz65              regul_kz1    -3.4221901E-02   -0.8202250        0.7860031         Cov. Mat.
 ippkz66              regul_kz1     7.6013841E-03   -0.5590441        0.5666455         Cov. Mat.
 ippkz67              regul_kz1    -1.4150443E-02    0.2043027       -0.2184531         Cov. Mat.
 ippkz68              regul_kz1     2.3516774E-02    -1.144403         1.167919         Cov. Mat.
 ippkz69              regul_kz1    -6.8532923E-03    0.3323652       -0.3392185         Cov. Mat.
 ippkz70              regul_kz1     2.9088123E-02    0.1988415       -0.1697534         Cov. Mat.
 ippkz71              regul_kz1    -2.2508502E-03   -0.2410487        0.2387978         Cov. Mat.
 ippkz72              regul_kz1    -7.8739705E-02   -0.9968053        0.9180656         Cov. Mat.
 ippkz73              regul_kz1    -7.0206384E-03    0.2777835       -0.2848042         Cov. Mat.
 ippkz74              regul_kz1    -6.0842981E-02   -0.4211331        0.3602901         Cov. Mat.
 ippkz75              regul_kz1    -4.4208097E-02    0.7844068       -0.8286149         Cov. Mat.
 ippkz76              regul_kz1    -8.2115131E-02   -0.4967156        0.4146004         Cov. Mat.
 ippkz77              regul_kz1    -4.8505506E-02   -0.3784008        0.3298953         Cov. Mat.
 ippkz78              regul_kz1    -2.3342887E-02   -0.2109646        0.1876217         Cov. Mat.
 ippkz79              regul_kz1     1.1590566E-02    0.9581724       -0.9465819         Cov. Mat.
 ippkz80              regul_kz1    -7.6097402E-02   -0.1562003        8.0102904E-02     Cov. Mat.
 ippkz81              regul_kz1     2.8110244E-02   -8.5403297E-03    3.6650574E-02     Cov. Mat.
 ippkz82              regul_kz1     3.8957233E-02   -0.2653298        0.3042871         Cov. Mat.
 ippkz83              regul_kz1     0.3316378        0.5121191       -0.1804813         Cov. Mat.
 ippkz84              regul_kz1    -9.5964986E-02   -0.1522880        5.6323042E-02     Cov. Mat.
 ippkz85              regul_kz1    -0.2924749        0.1751770       -0.4676519         Cov. Mat.
 ippkz86              regul_kz1    -0.2993211       -0.2171519       -8.2169188E-02     Cov. Mat.
 ippkz87              regul_kz1    -0.1960369        1.2681484E-02   -0.2087184         Cov. Mat.
 ippkz88              regul_kz2     3.7201058E-03    6.8756868E-02   -6.5036763E-02     Cov. Mat.
 ippkz89              regul_kz2     3.7565443E-03   -3.2085997E-02    3.5842541E-02     Cov. Mat.
 ippkz90              regul_kz2     2.2333191E-04   -0.4835028        0.4837262         Cov. Mat.
 ippkz91              regul_kz2    -1.6096426E-04   -0.2147145        0.2145535         Cov. Mat.
 ippkz92              regul_kz2     1.3895445E-03   -0.5707766        0.5721662         Cov. Mat.
 ippkz93              regul_kz2     2.7723043E-04   -0.1980057        0.1982830         Cov. Mat.
 ippkz94              regul_kz2     4.3481193E-03   -0.5833249        0.5876730         Cov. Mat.
 ippkz95              regul_kz2     1.8855163E-03   -0.7282748        0.7301604         Cov. Mat.
 ippkz96              regul_kz2     1.0828579E-04   -0.8623928        0.8625010         Cov. Mat.
 ippkz97              regul_kz2     5.3864021E-03   -0.3703163        0.3757027         Cov. Mat.
 ippkz98              regul_kz2    -4.0047288E-03   -0.3632856        0.3592809         Cov. Mat.
 ippkz99              regul_kz2     7.1858515E-03    0.6610837       -0.6538979         Cov. Mat.
 ippkz100             regul_kz2     2.2940599E-03    0.2174168       -0.2151227         Cov. Mat.
 ippkz101             regul_kz2     5.8620358E-04   -0.1332034        0.1337896         Cov. Mat.
 ippkz102             regul_kz2    -1.6415215E-04   -0.1478141        0.1476499         Cov. Mat.
 ippkz103             regul_kz2    -5.0864714E-03    0.5844489       -0.5895354         Cov. Mat.
 ippkz104             regul_kz2     2.1146593E-03   -6.3873693E-03    8.5020286E-03     Cov. Mat.
 ippkz105             regul_kz2    -9.8911930E-03    0.1592521       -0.1691432         Cov. Mat.
 ippkz106             regul_kz2     5.3771503E-03    0.2366124       -0.2312353         Cov. Mat.
 ippkz107             regul_kz2     7.9958884E-03   -8.3520795E-02    9.1516684E-02     Cov. Mat.
 ippkz108             regul_kz2     5.5708475E-03   -0.2550770        0.2606478         Cov. Mat.
 ippkz109             regul_kz2    -5.9157863E-03   -0.1769715        0.1710557         Cov. Mat.
 ippkz110             regul_kz2    -2.2909875E-03    0.1900204       -0.1923114         Cov. Mat.
 ippkz111             regul_kz2    -8.0965571E-03   -0.2941946        0.2860981         Cov. Mat.
 ippkz112             regul_kz2     1.9030797E-03    0.2042267       -0.2023236         Cov. Mat.
 ippkz113             regul_kz2    -5.7582557E-04    0.5669571       -0.5675330         Cov. Mat.
 ippkz114             regul_kz2    -6.4674141E-03   -6.1962312E-02    5.5494898E-02     Cov. Mat.
 ippkz115             regul_kz2     9.3406241E-04   -0.2104280        0.2113620         Cov. Mat.
 ippkz116             regul_kz2     2.1916399E-03   -0.1241440        0.1263356         Cov. Mat.
 ippkz117             regul_kz2    -2.2961820E-04    0.4216858       -0.4219154         Cov. Mat.
 ippkz118             regul_kz2     1.7136833E-03   -0.2920137        0.2937274         Cov. Mat.
 ippkz119             regul_kz2    -4.5301249E-03   -0.5077336        0.5032034         Cov. Mat.
 ippkz120             regul_kz2     3.6935633E-03    4.0884042E-02   -3.7190478E-02     Cov. Mat.
 ippkz121             regul_kz2    -1.5857289E-03    0.3564601       -0.3580459         Cov. Mat.
 ippkz122             regul_kz2    -1.0135404E-02   -0.5337894        0.5236540         Cov. Mat.
 ippkz123             regul_kz2     3.8340759E-03    0.2349420       -0.2311079         Cov. Mat.
 ippkz124             regul_kz2     1.0427630E-02   -0.3450794        0.3555070         Cov. Mat.
 ippkz125             regul_kz2     5.9205986E-04   -0.3991154        0.3997074         Cov. Mat.
 ippkz126             regul_kz2    -7.0151502E-03   -0.2709397        0.2639246         Cov. Mat.
 ippkz127             regul_kz2    -1.3345470E-02    0.2900123       -0.3033578         Cov. Mat.
 ippkz128             regul_kz2     1.0233429E-02   -3.2137891E-02    4.2371320E-02     Cov. Mat.
 ippkz129             regul_kz2     3.0472513E-02   -5.6808754E-02    8.7281268E-02     Cov. Mat.
 ippkz130             regul_kz2    -7.4290205E-04    4.3861572E-02   -4.4604474E-02     Cov. Mat.
 ippkz131             regul_kz2     5.0281890E-03    0.5238334       -0.5188052         Cov. Mat.
 ippkz132             regul_kz2    -3.1709980E-03   -0.1263245        0.1231535         Cov. Mat.
 ippkz133             regul_kz2    -1.0596187E-02    3.8998537E-03   -1.4496040E-02     Cov. Mat.
 ippkz134             regul_kz2    -1.1543671E-02    0.6412213       -0.6527650         Cov. Mat.
 ippkz135             regul_kz2     3.3081595E-03   -0.2280548        0.2313630         Cov. Mat.
 ippkz136             regul_kz2    -1.7475625E-03   -0.3014134        0.2996658         Cov. Mat.
 ippkz137             regul_kz2    -2.7642686E-03    0.8656931       -0.8684574         Cov. Mat.
 ippkz138             regul_kz2    -6.8479048E-04   -0.3846701        0.3839853         Cov. Mat.
 ippkz139             regul_kz2    -3.4389078E-03    0.3198469       -0.3232858         Cov. Mat.
 ippkz140             regul_kz2    -1.2198747E-02   -0.1615774        0.1493787         Cov. Mat.
 ippkz141             regul_kz2     6.3198722E-03    0.4088110       -0.4024912         Cov. Mat.
 ippkz142             regul_kz2    -1.6796134E-03    0.2910052       -0.2926848         Cov. Mat.
 ippkz143             regul_kz2    -2.2726291E-02    0.1435997       -0.1663260         Cov. Mat.
 ippkz144             regul_kz2     4.2790626E-02    0.9791045       -0.9363139         Cov. Mat.
 ippkz145             regul_kz2     2.8875156E-02    0.6523211       -0.6234459         Cov. Mat.
 ippkz146             regul_kz2     3.6184435E-02    0.2558836       -0.2196992         Cov. Mat.
 ippkz147             regul_kz2     2.7205105E-02    0.2495067       -0.2223016         Cov. Mat.
 ippkz148             regul_kz2    -3.1942209E-02    2.5929196E-02   -5.7871405E-02     Cov. Mat.
 ippkz149             regul_kz2     3.5155190E-02   -9.2385128E-02    0.1275403         Cov. Mat.
 ippkz150             regul_kz2     2.0363135E-02   -0.2108360        0.2311992         Cov. Mat.
 ippkz151             regul_kz2     1.2907734E-02    0.2924860       -0.2795782         Cov. Mat.
 ippkz152             regul_kz2     7.8452903E-03   -8.5696213E-02    9.3541503E-02     Cov. Mat.
 ippkz153             regul_kz2     4.2224032E-02    0.3290388       -0.2868148         Cov. Mat.
 ippkz154             regul_kz2     3.3358106E-02   -3.5164569E-03    3.6874563E-02     Cov. Mat.
 ippkz155             regul_kz2     2.0202335E-02   -0.1950613        0.2152636         Cov. Mat.
 ippkz156             regul_kz2    -3.2180036E-03   -0.3947123        0.3914943         Cov. Mat.
 ippkz157             regul_kz2     7.1449586E-02    0.6210604       -0.5496108         Cov. Mat.
 ippkz158             regul_kz2     7.7825490E-02   -0.2681535        0.3459790         Cov. Mat.
 ippkz159             regul_kz2     4.4503260E-02   -0.2493627        0.2938659         Cov. Mat.
 ippkz160             regul_kz2    -5.4111567E-03   -0.4130383        0.4076271         Cov. Mat.
 ippkz161             regul_kz2    -5.4865481E-02    7.5016534E-02   -0.1298820         Cov. Mat.
 ippkz162             regul_kz2    -4.4793821E-02    6.3448531E-02   -0.1082424         Cov. Mat.
 ippkz163             regul_kz2     1.4539477E-02    0.4105166       -0.3959771         Cov. Mat.
 ippkz164             regul_kz2    -9.5525319E-02   -0.2305072        0.1349818         Cov. Mat.
 ippkz165             regul_kz2     0.1303080        0.8230295       -0.6927215         Cov. Mat.
 ippkz166             regul_kz2    -0.1276922       -5.8621132E-02   -6.9071072E-02     Cov. Mat.
 ippkz167             regul_kz2    -0.2576550       -0.4276635        0.1700085         Cov. Mat.
 ippkz168             regul_kz2    -8.5565962E-02    0.2676637       -0.3532297         Cov. Mat.
 ippkz169             regul_kz2    -5.1877369E-03    0.6318747       -0.6370624         Cov. Mat.
 ippkz170             regul_kz2     9.4124278E-02   -0.7405692        0.8346934         Cov. Mat.
 ippkz171             regul_kz2     1.6734896E-02     1.277381        -1.260646         Cov. Mat.
 ippkz172             regul_kz2     7.5481163E-02    0.8199514       -0.7444703         Cov. Mat.
 ippkz173             regul_kz2     0.8137083         1.919121        -1.105413         Cov. Mat.
 ippkz174             regul_kz2     0.6174738        6.8412349E-02    0.5490615         Cov. Mat.
 ippkz175             regul_kz2     0.7874704       -0.4786690         1.266139         Cov. Mat.
 ippkz176             regul_kz2     0.1490710         1.077908       -0.9288372         Cov. Mat.
 ippkz177             regul_kz2     0.2108210         4.195890        -3.985069         Cov. Mat.
 ippkz178             regul_kz1     4.4615427E-02    0.6004165       -0.5558011         Cov. Mat.
 ippkz179             regul_kz1     5.2521033E-03   -0.2738763        0.2791284         Cov. Mat.
 ippkz180             regul_kz1    -7.2316217E-02    0.7163803       -0.7886966         Cov. Mat.
 ippkz181             regul_kz1     0.9100083         2.232976        -1.322967         Cov. Mat.
 ippkz182             regul_kz1     0.6592659         1.751791        -1.092525         Cov. Mat.
 ippkz183             regul_kz1     0.9567101       -8.5271233E-02     1.041981         Cov. Mat.
 ippkz184             regul_kz1    -0.2797522        0.4574792       -0.7372314         Cov. Mat.
 ippkz185             regul_kz1    -0.1382092        -1.182043         1.043834         Cov. Mat.
 ippkz186             regul_kz1     -9.661553        -11.79667         2.135120         Cov. Mat.
 ippkz187             regul_kz2     -9.439619        -21.55634         12.11672         Cov. Mat.
 ippss1               regul_ppss    -6.000000        -6.952884        0.9528839        5.0000000E-02
 ippss2               regul_ppss    -6.000000        -4.408157        -1.591843        5.0000000E-02
 ippss3               regul_ppss    -6.000000        -6.020249        2.0248610E-02    5.0000000E-02
 ippss4               regul_ppss    -6.000000        -5.732049       -0.2679507        5.0000000E-02
 ippss5               regul_ppss    -6.000000        -6.892100        0.8921001        5.0000000E-02
 ippss6               regul_ppss    -6.000000        -5.698975       -0.3010252        5.0000000E-02
 ippss7               regul_ppss    -6.000000        -6.866642        0.8666423        5.0000000E-02
 ippss8               regul_ppss    -6.000000        -5.685132       -0.3148679        5.0000000E-02
 ippss9               regul_ppss    -6.000000        -5.877763       -0.1222370        5.0000000E-02
 ippss10              regul_ppss    -6.000000        -5.972287       -2.7712999E-02    5.0000000E-02
 ippss11              regul_ppss    -6.000000        -5.391194       -0.6088060        5.0000000E-02
 ippss12              regul_ppss    -6.000000        -5.419032       -0.5809675        5.0000000E-02
 ippss13              regul_ppss    -6.000000        -5.578384       -0.4216159        5.0000000E-02
 ippss14              regul_ppss    -6.000000        -5.506096       -0.4939039        5.0000000E-02
 ippss15              regul_ppss    -6.000000        -5.897988       -0.1020124        5.0000000E-02
 ippss16              regul_ppss    -6.000000        -6.600539        0.6005392        5.0000000E-02
 ippss17              regul_ppss    -6.000000        -6.527403        0.5274028        5.0000000E-02
 ippss18              regul_ppss    -6.000000        -5.175674       -0.8243256        5.0000000E-02
 ippss19              regul_ppss    -6.000000        -6.140717        0.1407172        5.0000000E-02
 ippss20              regul_ppss    -6.000000        -4.570704        -1.429296        5.0000000E-02
 ippss21              regul_ppss    -6.000000        -6.096191        9.6190554E-02    5.0000000E-02
 ippss22              regul_ppss    -6.000000        -5.249654       -0.7503456        5.0000000E-02
 ippss23              regul_ppss    -6.000000        -4.607818        -1.392182        5.0000000E-02
 ippss24              regul_ppss    -6.000000        -5.122188       -0.8778118        5.0000000E-02
 ippss25              regul_ppss    -6.000000        -6.998854        0.9988537        5.0000000E-02
 ippss26              regul_ppss    -6.000000        -4.756209        -1.243791        5.0000000E-02
 ippss27              regul_ppss    -6.000000        -4.618482        -1.381518        5.0000000E-02
 ippss28              regul_ppss    -6.000000        -6.517190        0.5171904        5.0000000E-02
 ippss29              regul_ppss    -6.000000        -5.901388       -9.8611992E-02    5.0000000E-02
 ippss30              regul_ppss    -6.000000        -7.000000         1.000000        5.0000000E-02
 ippss31              regul_ppss    -6.000000        -6.167684        0.1676837        5.0000000E-02
 ippss32              regul_ppss    -6.000000        -5.203936       -0.7960641        5.0000000E-02
 ippss33              regul_ppss    -6.000000        -4.939596        -1.060404        5.0000000E-02
 ippss34              regul_ppss    -6.000000        -6.579315        0.5793150        5.0000000E-02
 ippss35              regul_ppss    -6.000000        -6.658741        0.6587411        5.0000000E-02
 ippss36              regul_ppss    -6.000000        -5.654857       -0.3451432        5.0000000E-02
 ippss37              regul_ppss    -6.000000        -6.520225        0.5202254        5.0000000E-02
 ippss38              regul_ppss    -6.000000        -6.293210        0.2932095        5.0000000E-02
 ippss39              regul_ppss    -6.000000        -6.164459        0.1644594        5.0000000E-02
 ippss40              regul_ppss    -6.000000        -6.174333        0.1743327        5.0000000E-02
 ippss41              regul_ppss    -6.000000        -5.475900       -0.5240997        5.0000000E-02
 ippss42              regul_ppss    -6.000000        -5.867327       -0.1326732        5.0000000E-02
 ippss43              regul_ppss    -6.000000        -6.208885        0.2088845        5.0000000E-02
 ippss44              regul_ppss    -6.000000        -6.792313        0.7923130        5.0000000E-02
 ippss45              regul_ppss    -6.000000        -6.043449        4.3449376E-02    5.0000000E-02
 ippss46              regul_ppss    -6.000000        -6.342590        0.3425903        5.0000000E-02
 ippss47              regul_ppss    -6.000000        -5.786110       -0.2138901        5.0000000E-02
 ippss48              regul_ppss    -6.000000        -5.540865       -0.4591353        5.0000000E-02
 ippss49              regul_ppss    -6.000000        -5.908183       -9.1817335E-02    5.0000000E-02
 ippss50              regul_ppss    -6.000000        -5.482600       -0.5174001        5.0000000E-02
 ippss51              regul_ppss    -6.000000        -6.539733        0.5397333        5.0000000E-02
 ippss52              regul_ppss    -6.000000        -5.847220       -0.1527804        5.0000000E-02
 ippss53              regul_ppss    -6.000000        -6.275768        0.2757683        5.0000000E-02
 ippss54              regul_ppss    -6.000000        -5.224547       -0.7754528        5.0000000E-02
 ippss55              regul_ppss    -6.000000        -6.626092        0.6260918        5.0000000E-02
 ippss56              regul_ppss    -6.000000        -5.874027       -0.1259733        5.0000000E-02
 ippss57              regul_ppss    -6.000000        -4.575349        -1.424651        5.0000000E-02
 ippss58              regul_ppss    -6.000000        -6.754876        0.7548765        5.0000000E-02
 ippss59              regul_ppss    -6.000000        -6.121157        0.1211573        5.0000000E-02
 ippss60              regul_ppss    -6.000000        -5.858905       -0.1410945        5.0000000E-02
 ippss61              regul_ppss    -6.000000        -6.412312        0.4123120        5.0000000E-02
 ippss62              regul_ppss    -6.000000        -5.897408       -0.1025919        5.0000000E-02
 ippss63              regul_ppss    -6.000000        -5.941594       -5.8406207E-02    5.0000000E-02
 ippss64              regul_ppss    -6.000000        -6.555997        0.5559968        5.0000000E-02
 ippss65              regul_ppss    -6.000000        -6.176588        0.1765875        5.0000000E-02
 ippss66              regul_ppss    -6.000000        -6.049782        4.9781574E-02    5.0000000E-02
 ippss67              regul_ppss    -6.000000        -5.442690       -0.5573095        5.0000000E-02
 ippss68              regul_ppss    -6.000000        -6.899353        0.8993528        5.0000000E-02
 ippss69              regul_ppss    -6.000000        -5.405306       -0.5946938        5.0000000E-02
 ippss70              regul_ppss    -6.000000        -5.574787       -0.4252127        5.0000000E-02
 ippss71              regul_ppss    -6.000000        -6.272509        0.2725088        5.0000000E-02
 ippss72              regul_ppss    -6.000000        -6.559235        0.5592345        5.0000000E-02
 ippss73              regul_ppss    -6.000000        -6.662928        0.6629280        5.0000000E-02
 ippss74              regul_ppss    -6.000000        -4.320167        -1.679833        5.0000000E-02
 ippss75              regul_ppss    -6.000000        -5.680981       -0.3190188        5.0000000E-02
 ippss76              regul_ppss    -6.000000        -6.523496        0.5234962        5.0000000E-02
 ippss77              regul_ppss    -6.000000        -6.349641        0.3496412        5.0000000E-02
 ippss78              regul_ppss    -6.000000        -6.801330        0.8013305        5.0000000E-02
 ippss79              regul_ppss    -6.000000        -5.620237       -0.3797631        5.0000000E-02
 ippss80              regul_ppss    -6.000000        -5.781857       -0.2181426        5.0000000E-02
 ippss81              regul_ppss    -6.000000        -6.461840        0.4618404        5.0000000E-02
 ippss82              regul_ppss    -6.000000        -5.561019       -0.4389813        5.0000000E-02
 ippss83              regul_ppss    -6.000000        -6.349843        0.3498435        5.0000000E-02
 ippss84              regul_ppss    -6.000000        -5.811635       -0.1883652        5.0000000E-02
 ippss85              regul_ppss    -6.000000        -5.618127       -0.3818728        5.0000000E-02
 ippss86              regul_ppss    -6.000000        -5.519522       -0.4804775        5.0000000E-02
 ippss87              regul_ppss    -6.000000        -6.999604        0.9996045        5.0000000E-02
 ippss88              regul_ppss    -6.000000        -6.326524        0.3265244        5.0000000E-02
 ippss89              regul_ppss    -6.000000        -5.941207       -5.8792628E-02    5.0000000E-02
 ippss90              regul_ppss    -6.000000        -5.476592       -0.5234083        5.0000000E-02
 ippss91              regul_ppss    -6.000000        -5.588623       -0.4113771        5.0000000E-02
 ippss92              regul_ppss    -6.000000        -5.789151       -0.2108486        5.0000000E-02
 ippss93              regul_ppss    -6.000000        -4.927934        -1.072066        5.0000000E-02
 ippss94              regul_ppss    -6.000000        -6.524059        0.5240585        5.0000000E-02
 ippss95              regul_ppss    -6.000000        -4.835635        -1.164365        5.0000000E-02
 ippss96              regul_ppss    -6.000000        -5.350594       -0.6494063        5.0000000E-02
 ippss97              regul_ppss    -6.000000        -6.284609        0.2846091        5.0000000E-02
 ippss98              regul_ppss    -6.000000        -5.448583       -0.5514167        5.0000000E-02
 ippss99              regul_ppss    -6.000000        -6.294727        0.2947268        5.0000000E-02
 ippss100             regul_ppss    -6.000000        -6.517793        0.5177933        5.0000000E-02
 ippss101             regul_ppss    -6.000000        -6.792725        0.7927251        5.0000000E-02
 ippss102             regul_ppss    -6.000000        -6.226165        0.2261647        5.0000000E-02
 ippss103             regul_ppss    -6.000000        -5.707545       -0.2924550        5.0000000E-02
 ippss104             regul_ppss    -6.000000        -6.645842        0.6458420        5.0000000E-02
 ippss105             regul_ppss    -6.000000        -6.489480        0.4894804        5.0000000E-02
 ippss106             regul_ppss    -6.000000        -6.387516        0.3875159        5.0000000E-02
 ippss107             regul_ppss    -6.000000        -5.733983       -0.2660169        5.0000000E-02
 ippss108             regul_ppss    -6.000000        -5.578412       -0.4215879        5.0000000E-02
 ippss109             regul_ppss    -6.000000        -6.139458        0.1394577        5.0000000E-02
 ippss110             regul_ppss    -6.000000        -6.299648        0.2996480        5.0000000E-02
 ippss111             regul_ppss    -6.000000        -5.656963       -0.3430373        5.0000000E-02
 ippss112             regul_ppss    -6.000000        -5.905695       -9.4304576E-02    5.0000000E-02
 ippss113             regul_ppss    -6.000000        -6.999791        0.9997915        5.0000000E-02
 ippss114             regul_ppss    -6.000000        -5.498618       -0.5013817        5.0000000E-02
 ippss115             regul_ppss    -6.000000        -6.589332        0.5893323        5.0000000E-02
 ippss116             regul_ppss    -6.000000        -5.042735       -0.9572649        5.0000000E-02
 ippss117             regul_ppss    -6.000000        -5.895259       -0.1047408        5.0000000E-02
 ippss118             regul_ppss    -6.000000        -5.098142       -0.9018576        5.0000000E-02
 ippss119             regul_ppss    -6.000000        -5.701909       -0.2980913        5.0000000E-02
 ippss120             regul_ppss    -6.000000        -4.438647        -1.561353        5.0000000E-02
 ippss121             regul_ppss    -6.000000        -5.974435       -2.5564726E-02    5.0000000E-02
 ippss122             regul_ppss    -6.000000        -6.452428        0.4524280        5.0000000E-02
 ippss123             regul_ppss    -6.000000        -6.427366        0.4273662        5.0000000E-02
 ippss124             regul_ppss    -6.000000        -5.720629       -0.2793711        5.0000000E-02
 ippss125             regul_ppss    -6.000000        -4.474410        -1.525590        5.0000000E-02
 ippss126             regul_ppss    -6.000000        -5.874894       -0.1251060        5.0000000E-02
 ippss127             regul_ppss    -6.000000        -5.589405       -0.4105946        5.0000000E-02
 ippss128             regul_ppss    -6.000000        -6.205852        0.2058516        5.0000000E-02
 ippss129             regul_ppss    -6.000000        -6.462697        0.4626968        5.0000000E-02
 ippss130             regul_ppss    -6.000000        -5.926499       -7.3501332E-02    5.0000000E-02
 ippss131             regul_ppss    -6.000000        -5.905336       -9.4663950E-02    5.0000000E-02
 ippss132             regul_ppss    -6.000000        -5.757405       -0.2425947        5.0000000E-02
 ippss133             regul_ppss    -6.000000        -5.061465       -0.9385353        5.0000000E-02
 ippss134             regul_ppss    -6.000000        -5.414746       -0.5852541        5.0000000E-02
 ippss135             regul_ppss    -6.000000        -6.312789        0.3127885        5.0000000E-02
 ippss136             regul_ppss    -6.000000        -5.454872       -0.5451284        5.0000000E-02
 ippss137             regul_ppss    -6.000000        -6.323169        0.3231687        5.0000000E-02
 ippss138             regul_ppss    -6.000000        -6.051844        5.1844327E-02    5.0000000E-02
 ippss139             regul_ppss    -6.000000        -5.134000       -0.8660001        5.0000000E-02
 ippss140             regul_ppss    -6.000000        -5.046181       -0.9538189        5.0000000E-02
 ippss141             regul_ppss    -6.000000        -5.361195       -0.6388053        5.0000000E-02
 ippss142             regul_ppss    -6.000000        -4.922464        -1.077536        5.0000000E-02
 ippss143             regul_ppss    -6.000000        -6.005682        5.6820157E-03    5.0000000E-02
 ippss144             regul_ppss    -6.000000        -6.969409        0.9694086        5.0000000E-02
 ippss145             regul_ppss    -6.000000        -6.043497        4.3496888E-02    5.0000000E-02
 ippss146             regul_ppss    -6.000000        -5.679212       -0.3207875        5.0000000E-02
 ippss147             regul_ppss    -6.000000        -6.692550        0.6925498        5.0000000E-02
 ippss148             regul_ppss    -6.000000        -6.085987        8.5987027E-02    5.0000000E-02
 ippss149             regul_ppss    -6.000000        -6.228537        0.2285373        5.0000000E-02
 ippss150             regul_ppss    -6.000000        -6.338592        0.3385916        5.0000000E-02
 ippss151             regul_ppss    -6.000000        -5.867361       -0.1326389        5.0000000E-02
 ippss152             regul_ppss    -6.000000        -6.486029        0.4860292        5.0000000E-02
 ippss153             regul_ppss    -6.000000        -5.809143       -0.1908572        5.0000000E-02
 ippss154             regul_ppss    -6.000000        -6.670740        0.6707401        5.0000000E-02
 ippss155             regul_ppss    -6.000000        -5.360777       -0.6392232        5.0000000E-02
 ippss156             regul_ppss    -6.000000        -6.535501        0.5355013        5.0000000E-02
 ippss157             regul_ppss    -6.000000        -6.827146        0.8271463        5.0000000E-02
 ippss158             regul_ppss    -6.000000        -5.202219       -0.7977813        5.0000000E-02
 ippss159             regul_ppss    -6.000000        -6.704993        0.7049928        5.0000000E-02
 ippss160             regul_ppss    -6.000000        -4.746441        -1.253559        5.0000000E-02
 ippss161             regul_ppss    -6.000000        -5.545793       -0.4542074        5.0000000E-02
 ippss162             regul_ppss    -6.000000        -6.995654        0.9956542        5.0000000E-02
 ippss163             regul_ppss    -6.000000        -6.333840        0.3338402        5.0000000E-02
 ippss164             regul_ppss    -6.000000        -6.998411        0.9984106        5.0000000E-02
 ippss165             regul_ppss    -6.000000        -5.882546       -0.1174540        5.0000000E-02
 ippss166             regul_ppss    -6.000000        -4.485341        -1.514659        5.0000000E-02
 ippss167             regul_ppss    -6.000000        -5.234159       -0.7658415        5.0000000E-02
 ippss168             regul_ppss    -6.000000        -5.480677       -0.5193232        5.0000000E-02
 ippss169             regul_ppss    -6.000000        -5.870223       -0.1297771        5.0000000E-02
 ippss170             regul_ppss    -6.000000        -6.273819        0.2738192        5.0000000E-02
 ippss171             regul_ppss    -6.000000        -5.991485       -8.5151354E-03    5.0000000E-02
 ippss172             regul_ppss    -6.000000        -6.681613        0.6816132        5.0000000E-02
 ippss173             regul_ppss    -6.000000        -6.113641        0.1136408        5.0000000E-02
 ippss174             regul_ppss    -6.000000        -6.583817        0.5838166        5.0000000E-02
 ippss175             regul_ppss    -6.000000        -5.576276       -0.4237244        5.0000000E-02
 ippss176             regul_ppss    -6.000000        -5.450069       -0.5499315        5.0000000E-02
 ippss177             regul_ppss    -6.000000        -6.202271        0.2022711        5.0000000E-02
 ippss178             regul_ppss    -6.000000        -5.201996       -0.7980040        5.0000000E-02
 ippss179             regul_ppss    -6.000000        -5.481644       -0.5183564        5.0000000E-02
 ippss180             regul_ppss    -6.000000        -6.704785        0.7047847        5.0000000E-02
 ippss181             regul_ppss    -6.000000        -6.166800        0.1668003        5.0000000E-02
 ippss182             regul_ppss    -6.000000        -5.750185       -0.2498150        5.0000000E-02
 ippss183             regul_ppss    -6.000000        -5.980093       -1.9906610E-02    5.0000000E-02
 ippss184             regul_ppss    -6.000000        -6.327573        0.3275727        5.0000000E-02
 ippss185             regul_ppss    -6.000000        -6.234773        0.2347728        5.0000000E-02
 ippsy1               regul_ppsy    -1.000000        -1.107260        0.1072597        5.0000000E-02
 ippsy2               regul_ppsy    -1.000000        -1.312534        0.3125341        5.0000000E-02
 ippsy3               regul_ppsy    -1.000000        -1.557133        0.5571330        5.0000000E-02
 ippsy4               regul_ppsy    -1.000000        -1.390821        0.3908212        5.0000000E-02
 ippsy5               regul_ppsy    -1.000000        -1.229135        0.2291346        5.0000000E-02
 ippsy6               regul_ppsy    -1.000000        -1.686011        0.6860115        5.0000000E-02
 ippsy7               regul_ppsy    -1.000000        -1.320202        0.3202024        5.0000000E-02
 ippsy8               regul_ppsy    -1.000000        -1.478758        0.4787578        5.0000000E-02
 ippsy9               regul_ppsy    -1.000000       -0.7555922       -0.2444078        5.0000000E-02
 ippsy10              regul_ppsy    -1.000000        -1.753092        0.7530916        5.0000000E-02
 ippsy11              regul_ppsy    -1.000000        -1.878993        0.8789934        5.0000000E-02
 ippsy12              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy13              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy14              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy15              regul_ppsy    -1.000000       -0.8440473       -0.1559527        5.0000000E-02
 ippsy16              regul_ppsy    -1.000000       -0.7215969       -0.2784031        5.0000000E-02
 ippsy17              regul_ppsy    -1.000000       -0.8661275       -0.1338725        5.0000000E-02
 ippsy18              regul_ppsy    -1.000000        -1.008339        8.3388906E-03    5.0000000E-02
 ippsy19              regul_ppsy    -1.000000       -0.9431755       -5.6824461E-02    5.0000000E-02
 ippsy20              regul_ppsy    -1.000000       -0.8050857       -0.1949143        5.0000000E-02
 ippsy21              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy22              regul_ppsy    -1.000000       -0.8504724       -0.1495276        5.0000000E-02
 ippsy23              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy24              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy25              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy26              regul_ppsy    -1.000000        -1.382346        0.3823459        5.0000000E-02
 ippsy27              regul_ppsy    -1.000000       -0.8918461       -0.1081539        5.0000000E-02
 ippsy28              regul_ppsy    -1.000000        -1.000974        9.7427645E-04    5.0000000E-02
 ippsy29              regul_ppsy    -1.000000        -2.471964         1.471964        5.0000000E-02
 ippsy30              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy31              regul_ppsy    -1.000000        -1.423920        0.4239200        5.0000000E-02
 ippsy32              regul_ppsy    -1.000000        -1.492946        0.4929462        5.0000000E-02
 ippsy33              regul_ppsy    -1.000000        -1.749358        0.7493580        5.0000000E-02
 ippsy34              regul_ppsy    -1.000000       -0.9722310       -2.7769019E-02    5.0000000E-02
 ippsy35              regul_ppsy    -1.000000        -2.496413         1.496413        5.0000000E-02
 ippsy36              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy37              regul_ppsy    -1.000000       -0.9201794       -7.9820636E-02    5.0000000E-02
 ippsy38              regul_ppsy    -1.000000       -0.7329666       -0.2670334        5.0000000E-02
 ippsy39              regul_ppsy    -1.000000       -0.8156010       -0.1843990        5.0000000E-02
 ippsy40              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy41              regul_ppsy    -1.000000        -1.371331        0.3713314        5.0000000E-02
 ippsy42              regul_ppsy    -1.000000       -0.8472235       -0.1527765        5.0000000E-02
 ippsy43              regul_ppsy    -1.000000        -1.468266        0.4682660        5.0000000E-02
 ippsy44              regul_ppsy    -1.000000        -1.554952        0.5549520        5.0000000E-02
 ippsy45              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy46              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy47              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy48              regul_ppsy    -1.000000        -2.174627         1.174627        5.0000000E-02
 ippsy49              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy50              regul_ppsy    -1.000000        -1.215309        0.2153092        5.0000000E-02
 ippsy51              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy52              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy53              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy54              regul_ppsy    -1.000000        -1.290508        0.2905076        5.0000000E-02
 ippsy55              regul_ppsy    -1.000000        -1.488708        0.4887081        5.0000000E-02
 ippsy56              regul_ppsy    -1.000000       -0.9007553       -9.9244747E-02    5.0000000E-02
 ippsy57              regul_ppsy    -1.000000        -1.654900        0.6549000        5.0000000E-02
 ippsy58              regul_ppsy    -1.000000       -0.8284681       -0.1715319        5.0000000E-02
 ippsy59              regul_ppsy    -1.000000       -0.7983298       -0.2016702        5.0000000E-02
 ippsy60              regul_ppsy    -1.000000        -3.819359         2.819359        5.0000000E-02
 ippsy61              regul_ppsy    -1.000000        -1.271569        0.2715693        5.0000000E-02
 ippsy62              regul_ppsy    -1.000000        -1.025903        2.5903116E-02    5.0000000E-02
 ippsy63              regul_ppsy    -1.000000       -0.7073189       -0.2926811        5.0000000E-02
 ippsy64              regul_ppsy    -1.000000       -0.6995306       -0.3004694        5.0000000E-02
 ippsy65              regul_ppsy    -1.000000        -1.686159        0.6861588        5.0000000E-02
 ippsy66              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy67              regul_ppsy    -1.000000       -0.8358489       -0.1641511        5.0000000E-02
 ippsy68              regul_ppsy    -1.000000       -0.9815575       -1.8442508E-02    5.0000000E-02
 ippsy69              regul_ppsy    -1.000000       -0.8146126       -0.1853874        5.0000000E-02
 ippsy70              regul_ppsy    -1.000000       -0.9140512       -8.5948836E-02    5.0000000E-02
 ippsy71              regul_ppsy    -1.000000        -1.359737        0.3597368        5.0000000E-02
 ippsy72              regul_ppsy    -1.000000        -1.233224        0.2332240        5.0000000E-02
 ippsy73              regul_ppsy    -1.000000       -0.7955129       -0.2044871        5.0000000E-02
 ippsy74              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy75              regul_ppsy    -1.000000       -0.9213312       -7.8668838E-02    5.0000000E-02
 ippsy76              regul_ppsy    -1.000000        -1.280172        0.2801722        5.0000000E-02
 ippsy77              regul_ppsy    -1.000000        -1.331110        0.3311102        5.0000000E-02
 ippsy79              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy80              regul_ppsy    -1.000000        -1.047126        4.7126182E-02    5.0000000E-02
 ippsy81              regul_ppsy    -1.000000       -0.8275118       -0.1724882        5.0000000E-02
 ippsy82              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy83              regul_ppsy    -1.000000        -1.068025        6.8025022E-02    5.0000000E-02
 ippsy84              regul_ppsy    -1.000000        -2.317039         1.317039        5.0000000E-02
 ippsy85              regul_ppsy    -1.000000       -0.7004047       -0.2995953        5.0000000E-02
 ippsy86              regul_ppsy    -1.000000        -1.413880        0.4138799        5.0000000E-02
 ippsy87              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy88              regul_ppsy    -1.000000        -1.004707        4.7070853E-03    5.0000000E-02
 ippsy89              regul_ppsy    -1.000000        -1.169328        0.1693281        5.0000000E-02
 ippsy91              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02
 ippsy92              regul_ppsy    -1.000000        -1.553944        0.5539437        5.0000000E-02
 ippsy93              regul_ppsy    -1.000000        -1.545849        0.5458487        5.0000000E-02
