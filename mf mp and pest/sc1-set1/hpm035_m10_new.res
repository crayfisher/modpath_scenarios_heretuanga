 Name                 Group          Measured         Modelled         Residual         Weight       Weight*Measured    Weight*Modelled  Weight*Residual  Measurement_sd   Natural_weight
 obs_one              grp_one        1.000000         1.000000         0.000000         1.000000         1.000000         1.000000         0.000000            na               na       
 irch                 regul_rch      1.000000         100.5933        -99.59330        5.0000000E-02    5.0000000E-02     5.029665        -4.979665            na               na       
 iq                   regul_q        1.000000         99.43206        -98.43206        5.0000000E-02    5.0000000E-02     4.971603        -4.921603            na               na       
 ippkx1               regul_kp1      1.700000         2.666161       -0.9661608         Cov. Mat.           na               na               na               na               na
 ippkx2               regul_kp1      2.700000         1.942810        0.7571903         Cov. Mat.           na               na               na               na               na
 ippkx3               regul_kp1      1.700000        0.3746617         1.325338         Cov. Mat.           na               na               na               na               na
 ippkx4               regul_kp1      1.700000        3.0533743E-02     1.669466         Cov. Mat.           na               na               na               na               na
 ippkx5               regul_kp1      2.700000         2.219028        0.4809717         Cov. Mat.           na               na               na               na               na
 ippkx6               regul_kp1      2.700000         2.931607       -0.2316066         Cov. Mat.           na               na               na               na               na
 ippkx7               regul_kp1      1.700000        0.9468963        0.7531037         Cov. Mat.           na               na               na               na               na
 ippkx8               regul_kp1      1.700000        5.3773527E-02     1.646226         Cov. Mat.           na               na               na               na               na
 ippkx9               regul_kp1      2.700000         1.026438         1.673562         Cov. Mat.           na               na               na               na               na
 ippkx10              regul_kp1      1.700000        0.6192386         1.080761         Cov. Mat.           na               na               na               na               na
 ippkx11              regul_kp1      1.700000        0.9292120        0.7707880         Cov. Mat.           na               na               na               na               na
 ippkx12              regul_kp1      2.700000         1.915081        0.7849195         Cov. Mat.           na               na               na               na               na
 ippkx13              regul_kp1      3.300000         2.638220        0.6617803         Cov. Mat.           na               na               na               na               na
 ippkx14              regul_kp1      2.700000         1.944059        0.7559411         Cov. Mat.           na               na               na               na               na
 ippkx15              regul_kp1      2.700000         3.780241        -1.080241         Cov. Mat.           na               na               na               na               na
 ippkx16              regul_kp1      3.300000         4.000000       -0.7000000         Cov. Mat.           na               na               na               na               na
 ippkx17              regul_kp1      2.700000        0.8313596         1.868640         Cov. Mat.           na               na               na               na               na
 ippkx18              regul_kp1      2.700000         1.111649         1.588351         Cov. Mat.           na               na               na               na               na
 ippkx19              regul_kp1      3.300000         3.812179       -0.5121788         Cov. Mat.           na               na               na               na               na
 ippkx20              regul_kp1      2.700000         2.396280        0.3037201         Cov. Mat.           na               na               na               na               na
 ippkx21              regul_kp1      2.700000         3.630006       -0.9300064         Cov. Mat.           na               na               na               na               na
 ippkx22              regul_kp1      2.700000         2.566006        0.1339938         Cov. Mat.           na               na               na               na               na
 ippkx23              regul_kp1      1.700000         1.313047        0.3869534         Cov. Mat.           na               na               na               na               na
 ippkx24              regul_kp1      3.300000         4.000000       -0.7000000         Cov. Mat.           na               na               na               na               na
 ippkx25              regul_kp1      2.700000         1.979928        0.7200724         Cov. Mat.           na               na               na               na               na
 ippkx26              regul_kp1      2.700000         1.720955        0.9790446         Cov. Mat.           na               na               na               na               na
 ippkx27              regul_kp1      3.300000         2.374659        0.9253411         Cov. Mat.           na               na               na               na               na
 ippkx28              regul_kp1      3.300000         2.097792         1.202208         Cov. Mat.           na               na               na               na               na
 ippkx29              regul_kp1      3.300000         2.423846        0.8761541         Cov. Mat.           na               na               na               na               na
 ippkx30              regul_kp1      2.700000        0.3787566         2.321243         Cov. Mat.           na               na               na               na               na
 ippkx31              regul_kp1      2.700000        0.2481374         2.451863         Cov. Mat.           na               na               na               na               na
 ippkx32              regul_kp1      2.700000         1.868741        0.8312594         Cov. Mat.           na               na               na               na               na
 ippkx33              regul_kp1      2.700000         2.213866        0.4861339         Cov. Mat.           na               na               na               na               na
 ippkx34              regul_kp1      2.700000         3.365414       -0.6654137         Cov. Mat.           na               na               na               na               na
 ippkx35              regul_kp1      2.700000         2.809897       -0.1098974         Cov. Mat.           na               na               na               na               na
 ippkx36              regul_kp1      3.300000         1.363373         1.936627         Cov. Mat.           na               na               na               na               na
 ippkx37              regul_kp1      2.700000         2.100385        0.5996150         Cov. Mat.           na               na               na               na               na
 ippkx38              regul_kp1      2.700000        0.9284744         1.771526         Cov. Mat.           na               na               na               na               na
 ippkx39              regul_kp1      2.700000         3.195144       -0.4951435         Cov. Mat.           na               na               na               na               na
 ippkx40              regul_kp1      3.300000         1.257031         2.042969         Cov. Mat.           na               na               na               na               na
 ippkx41              regul_kp1      3.300000         2.907985        0.3920151         Cov. Mat.           na               na               na               na               na
 ippkx42              regul_kp1      2.700000         2.640060        5.9940043E-02     Cov. Mat.           na               na               na               na               na
 ippkx43              regul_kp1      2.700000         1.053330         1.646670         Cov. Mat.           na               na               na               na               na
 ippkx44              regul_kp1      1.700000         1.888954       -0.1889543         Cov. Mat.           na               na               na               na               na
 ippkx45              regul_kp1      2.700000         2.090387        0.6096132         Cov. Mat.           na               na               na               na               na
 ippkx46              regul_kp1      2.700000         2.736119       -3.6118624E-02     Cov. Mat.           na               na               na               na               na
 ippkx47              regul_kp1      3.300000         2.426782        0.8732177         Cov. Mat.           na               na               na               na               na
 ippkx48              regul_kp1      2.700000         1.909144        0.7908561         Cov. Mat.           na               na               na               na               na
 ippkx49              regul_kp1      3.300000         2.889180        0.4108195         Cov. Mat.           na               na               na               na               na
 ippkx50              regul_kp1      3.300000         1.671459         1.628541         Cov. Mat.           na               na               na               na               na
 ippkx51              regul_kp1      3.300000         2.000367         1.299633         Cov. Mat.           na               na               na               na               na
 ippkx52              regul_kp1      2.700000         2.729161       -2.9160981E-02     Cov. Mat.           na               na               na               na               na
 ippkx53              regul_kp1      2.700000         1.258734         1.441266         Cov. Mat.           na               na               na               na               na
 ippkx54              regul_kp1      2.700000         1.758344        0.9416560         Cov. Mat.           na               na               na               na               na
 ippkx55              regul_kp1      2.700000         1.610187         1.089813         Cov. Mat.           na               na               na               na               na
 ippkx56              regul_kp1      2.700000         2.792104       -9.2104470E-02     Cov. Mat.           na               na               na               na               na
 ippkx57              regul_kp1      2.700000         1.488830         1.211170         Cov. Mat.           na               na               na               na               na
 ippkx58              regul_kp1      3.300000         1.606159         1.693841         Cov. Mat.           na               na               na               na               na
 ippkx59              regul_kp1      3.300000         2.884068        0.4159322         Cov. Mat.           na               na               na               na               na
 ippkx60              regul_kp1      3.300000         1.914131         1.385869         Cov. Mat.           na               na               na               na               na
 ippkx61              regul_kp1      3.300000        0.1533032         3.146697         Cov. Mat.           na               na               na               na               na
 ippkx62              regul_kp1      2.700000         2.277299        0.4227010         Cov. Mat.           na               na               na               na               na
 ippkx63              regul_kp1      2.700000         2.307529        0.3924714         Cov. Mat.           na               na               na               na               na
 ippkx64              regul_kp1      1.700000         1.564118        0.1358816         Cov. Mat.           na               na               na               na               na
 ippkx65              regul_kp1      2.700000         2.957980       -0.2579795         Cov. Mat.           na               na               na               na               na
 ippkx66              regul_kp1      3.300000         1.405756         1.894244         Cov. Mat.           na               na               na               na               na
 ippkx67              regul_kp1      3.300000         2.548517        0.7514833         Cov. Mat.           na               na               na               na               na
 ippkx68              regul_kp1      3.300000         2.299254         1.000746         Cov. Mat.           na               na               na               na               na
 ippkx69              regul_kp1      3.300000         2.830798        0.4692019         Cov. Mat.           na               na               na               na               na
 ippkx70              regul_kp1      3.300000         2.942782        0.3572181         Cov. Mat.           na               na               na               na               na
 ippkx71              regul_kp1      3.300000         2.612744        0.6872558         Cov. Mat.           na               na               na               na               na
 ippkx72              regul_kp1      2.700000         1.631567         1.068433         Cov. Mat.           na               na               na               na               na
 ippkx73              regul_kp1      2.700000         2.109421        0.5905791         Cov. Mat.           na               na               na               na               na
 ippkx74              regul_kp1      1.700000         1.280345        0.4196550         Cov. Mat.           na               na               na               na               na
 ippkx75              regul_kp1      2.700000         3.887496        -1.187496         Cov. Mat.           na               na               na               na               na
 ippkx76              regul_kp1      3.300000         1.743826         1.556174         Cov. Mat.           na               na               na               na               na
 ippkx77              regul_kp1      3.300000         1.704966         1.595034         Cov. Mat.           na               na               na               na               na
 ippkx78              regul_kp1      3.300000         1.985266         1.314734         Cov. Mat.           na               na               na               na               na
 ippkx79              regul_kp1      3.300000         2.684545        0.6154554         Cov. Mat.           na               na               na               na               na
 ippkx80              regul_kp1      3.300000         2.178067         1.121933         Cov. Mat.           na               na               na               na               na
 ippkx81              regul_kp1      2.700000         1.677929         1.022071         Cov. Mat.           na               na               na               na               na
 ippkx82              regul_kp1      2.700000         1.384133         1.315867         Cov. Mat.           na               na               na               na               na
 ippkx83              regul_kp1      2.700000        0.9271187         1.772881         Cov. Mat.           na               na               na               na               na
 ippkx84              regul_kp1      3.300000         3.444959       -0.1449593         Cov. Mat.           na               na               na               na               na
 ippkx85              regul_kp1      2.700000         2.166210        0.5337896         Cov. Mat.           na               na               na               na               na
 ippkx86              regul_kp1      2.700000         1.956103        0.7438971         Cov. Mat.           na               na               na               na               na
 ippkx87              regul_kp1      3.300000         1.955791         1.344209         Cov. Mat.           na               na               na               na               na
 ippkx88              regul_kp1      3.300000         3.192043        0.1079566         Cov. Mat.           na               na               na               na               na
 ippkx89              regul_kp1      3.300000         2.578148        0.7218516         Cov. Mat.           na               na               na               na               na
 ippkx90              regul_kp1      3.300000         3.455176       -0.1551765         Cov. Mat.           na               na               na               na               na
 ippkx91              regul_kp1      2.700000         2.235282        0.4647178         Cov. Mat.           na               na               na               na               na
 ippkx92              regul_kp1      2.700000         1.796049        0.9039509         Cov. Mat.           na               na               na               na               na
 ippkx93              regul_kp1      2.700000        1.2763228E-15     2.700000         Cov. Mat.           na               na               na               na               na
 ippkx94              regul_kp1      2.700000        0.9482358         1.751764         Cov. Mat.           na               na               na               na               na
 ippkx95              regul_kp1      2.700000         1.382902         1.317098         Cov. Mat.           na               na               na               na               na
 ippkx96              regul_kp1      2.700000         1.040718         1.659282         Cov. Mat.           na               na               na               na               na
 ippkx97              regul_kp1      3.300000         1.772288         1.527712         Cov. Mat.           na               na               na               na               na
 ippkx98              regul_kp1      1.700000         1.876104       -0.1761044         Cov. Mat.           na               na               na               na               na
 ippkx99              regul_kp2      2.700000        1.7663822E-15     2.700000         Cov. Mat.           na               na               na               na               na
 ippkx100             regul_kp2      2.700000         1.959994        0.7400064         Cov. Mat.           na               na               na               na               na
 ippkx101             regul_kp2      2.700000        0.3661027         2.333897         Cov. Mat.           na               na               na               na               na
 ippkx102             regul_kp2      2.700000         1.848280        0.8517197         Cov. Mat.           na               na               na               na               na
 ippkx103             regul_kp2      2.700000        0.9023076         1.797692         Cov. Mat.           na               na               na               na               na
 ippkx104             regul_kp2      2.700000        1.4542404E-15     2.700000         Cov. Mat.           na               na               na               na               na
 ippkx105             regul_kp2      2.700000         2.201460        0.4985395         Cov. Mat.           na               na               na               na               na
 ippkx106             regul_kp2      2.700000         2.051293        0.6487074         Cov. Mat.           na               na               na               na               na
 ippkx107             regul_kp2      2.700000         2.630521        6.9478970E-02     Cov. Mat.           na               na               na               na               na
 ippkx108             regul_kp2      2.700000         1.414164         1.285836         Cov. Mat.           na               na               na               na               na
 ippkx109             regul_kp2      2.700000         3.133196       -0.4331958         Cov. Mat.           na               na               na               na               na
 ippkx110             regul_kp2      2.700000         3.867406        -1.167406         Cov. Mat.           na               na               na               na               na
 ippkx111             regul_kp2      2.700000         1.565838         1.134162         Cov. Mat.           na               na               na               na               na
 ippkx112             regul_kp2      2.700000        0.9339301         1.766070         Cov. Mat.           na               na               na               na               na
 ippkx113             regul_kp2      2.700000         3.148079       -0.4480789         Cov. Mat.           na               na               na               na               na
 ippkx114             regul_kp2      2.700000         2.096233        0.6037673         Cov. Mat.           na               na               na               na               na
 ippkx115             regul_kp2      2.700000         1.615912         1.084088         Cov. Mat.           na               na               na               na               na
 ippkx116             regul_kp2      2.700000         3.297327       -0.5973269         Cov. Mat.           na               na               na               na               na
 ippkx117             regul_kp2      2.700000         2.396543        0.3034570         Cov. Mat.           na               na               na               na               na
 ippkx118             regul_kp2      2.700000         1.629620         1.070380         Cov. Mat.           na               na               na               na               na
 ippkx119             regul_kp2      2.700000         2.064883        0.6351169         Cov. Mat.           na               na               na               na               na
 ippkx120             regul_kp2      2.700000         1.261784         1.438216         Cov. Mat.           na               na               na               na               na
 ippkx121             regul_kp2      2.700000         2.625276        7.4724085E-02     Cov. Mat.           na               na               na               na               na
 ippkx122             regul_kp2      2.700000         1.786538        0.9134620         Cov. Mat.           na               na               na               na               na
 ippkx123             regul_kp2      2.700000         2.511283        0.1887166         Cov. Mat.           na               na               na               na               na
 ippkx124             regul_kp2      2.700000         2.214473        0.4855265         Cov. Mat.           na               na               na               na               na
 ippkx125             regul_kp2      2.700000         3.138048       -0.4380484         Cov. Mat.           na               na               na               na               na
 ippkx126             regul_kp2      2.700000         3.070069       -0.3700689         Cov. Mat.           na               na               na               na               na
 ippkx127             regul_kp2      2.700000        0.2116000         2.488400         Cov. Mat.           na               na               na               na               na
 ippkx128             regul_kp2      2.700000        0.9820973         1.717903         Cov. Mat.           na               na               na               na               na
 ippkx129             regul_kp2      2.700000        0.1952674         2.504733         Cov. Mat.           na               na               na               na               na
 ippkx130             regul_kp2      2.700000         2.610244        8.9756129E-02     Cov. Mat.           na               na               na               na               na
 ippkx131             regul_kp2      2.700000         1.462198         1.237802         Cov. Mat.           na               na               na               na               na
 ippkx132             regul_kp2      2.700000         2.375285        0.3247149         Cov. Mat.           na               na               na               na               na
 ippkx133             regul_kp2      2.700000         2.669189        3.0811009E-02     Cov. Mat.           na               na               na               na               na
 ippkx134             regul_kp2      2.700000         2.312914        0.3870863         Cov. Mat.           na               na               na               na               na
 ippkx135             regul_kp2      2.700000         3.483420       -0.7834203         Cov. Mat.           na               na               na               na               na
 ippkx136             regul_kp2      2.700000         2.672870        2.7130431E-02     Cov. Mat.           na               na               na               na               na
 ippkx137             regul_kp2      2.700000         1.590353         1.109647         Cov. Mat.           na               na               na               na               na
 ippkx138             regul_kp2      2.700000         2.358842        0.3411577         Cov. Mat.           na               na               na               na               na
 ippkx139             regul_kp2      2.700000         1.105994         1.594006         Cov. Mat.           na               na               na               na               na
 ippkx140             regul_kp2      2.700000         1.804865        0.8951355         Cov. Mat.           na               na               na               na               na
 ippkx141             regul_kp2      2.700000         1.424809         1.275191         Cov. Mat.           na               na               na               na               na
 ippkx142             regul_kp2      2.700000         1.395854         1.304146         Cov. Mat.           na               na               na               na               na
 ippkx143             regul_kp2      2.700000        0.2169833         2.483017         Cov. Mat.           na               na               na               na               na
 ippkx144             regul_kp2      2.700000        0.4984094         2.201591         Cov. Mat.           na               na               na               na               na
 ippkx145             regul_kp2      2.700000         2.068767        0.6312329         Cov. Mat.           na               na               na               na               na
 ippkx146             regul_kp2      2.700000         3.618956       -0.9189556         Cov. Mat.           na               na               na               na               na
 ippkx147             regul_kp2      2.700000         2.002935        0.6970646         Cov. Mat.           na               na               na               na               na
 ippkx148             regul_kp2      2.700000         2.729103       -2.9103449E-02     Cov. Mat.           na               na               na               na               na
 ippkx149             regul_kp2      2.700000        0.9185127         1.781487         Cov. Mat.           na               na               na               na               na
 ippkx150             regul_kp2      2.700000         1.029245         1.670755         Cov. Mat.           na               na               na               na               na
 ippkx151             regul_kp2      2.700000         1.600382         1.099618         Cov. Mat.           na               na               na               na               na
 ippkx152             regul_kp2      2.700000         1.666872         1.033128         Cov. Mat.           na               na               na               na               na
 ippkx153             regul_kp2      2.700000         2.513497        0.1865030         Cov. Mat.           na               na               na               na               na
 ippkx154             regul_kp2      2.700000         1.589582         1.110418         Cov. Mat.           na               na               na               na               na
 ippkx155             regul_kp2      2.700000         3.774169        -1.074169         Cov. Mat.           na               na               na               na               na
 ippkx156             regul_kp2      2.700000         3.509149       -0.8091489         Cov. Mat.           na               na               na               na               na
 ippkx157             regul_kp2      2.700000        0.4387306         2.261269         Cov. Mat.           na               na               na               na               na
 ippkx158             regul_kp2      2.700000        1.3110173E-15     2.700000         Cov. Mat.           na               na               na               na               na
 ippkx159             regul_kp2      2.700000         2.236127        0.4638733         Cov. Mat.           na               na               na               na               na
 ippkx160             regul_kp2      2.700000         1.835023        0.8649766         Cov. Mat.           na               na               na               na               na
 ippkx161             regul_kp2      2.700000         2.150668        0.5493317         Cov. Mat.           na               na               na               na               na
 ippkx162             regul_kp2      2.700000         2.166998        0.5330019         Cov. Mat.           na               na               na               na               na
 ippkx163             regul_kp2      2.700000        2.0231213E-16     2.700000         Cov. Mat.           na               na               na               na               na
 ippkx164             regul_kp2      2.700000         2.906876       -0.2068763         Cov. Mat.           na               na               na               na               na
 ippkx165             regul_kp2      2.700000         3.078105       -0.3781054         Cov. Mat.           na               na               na               na               na
 ippkx166             regul_kp2      2.700000         2.284428        0.4155717         Cov. Mat.           na               na               na               na               na
 ippkx167             regul_kp2      2.700000         2.142749        0.5572505         Cov. Mat.           na               na               na               na               na
 ippkx168             regul_kp2      2.700000         1.897173        0.8028270         Cov. Mat.           na               na               na               na               na
 ippkx169             regul_kp2      2.700000         2.855477       -0.1554771         Cov. Mat.           na               na               na               na               na
 ippkx170             regul_kp2      2.700000         3.897977        -1.197977         Cov. Mat.           na               na               na               na               na
 ippkx171             regul_kp2      2.700000         1.721576        0.9784241         Cov. Mat.           na               na               na               na               na
 ippkx172             regul_kp2      2.700000        0.6022757         2.097724         Cov. Mat.           na               na               na               na               na
 ippkx173             regul_kp2      2.700000         1.532972         1.167028         Cov. Mat.           na               na               na               na               na
 ippkx174             regul_kp2      2.700000        0.8729129         1.827087         Cov. Mat.           na               na               na               na               na
 ippkx175             regul_kp2      2.700000        7.9678018E-16     2.700000         Cov. Mat.           na               na               na               na               na
 ippkx176             regul_kp2      2.700000        0.7486442         1.951356         Cov. Mat.           na               na               na               na               na
 ippkx177             regul_kp2      2.700000        9.3020829E-02     2.606979         Cov. Mat.           na               na               na               na               na
 ippkx178             regul_kp2      2.700000         4.000000        -1.300000         Cov. Mat.           na               na               na               na               na
 ippkx179             regul_kp2      2.700000         3.633024       -0.9330238         Cov. Mat.           na               na               na               na               na
 ippkx180             regul_kp2      2.700000         2.244670        0.4553298         Cov. Mat.           na               na               na               na               na
 ippkx181             regul_kp2      2.700000         2.233692        0.4663081         Cov. Mat.           na               na               na               na               na
 ippkx182             regul_kp2      2.700000        1.2488925E-15     2.700000         Cov. Mat.           na               na               na               na               na
 ippkx183             regul_kp2      2.700000         2.436385        0.2636155         Cov. Mat.           na               na               na               na               na
 ippkx184             regul_kp1      2.700000         2.138082        0.5619181         Cov. Mat.           na               na               na               na               na
 ippkx185             regul_kp1      2.700000         2.408877        0.2911231         Cov. Mat.           na               na               na               na               na
 ippkx186             regul_kp1      2.700000         1.784683        0.9153165         Cov. Mat.           na               na               na               na               na
 ippkx187             regul_kp1      2.700000        0.5305195         2.169481         Cov. Mat.           na               na               na               na               na
 ippkx188             regul_kp1      2.700000        0.8291426         1.870857         Cov. Mat.           na               na               na               na               na
 ippkx189             regul_kp1      2.700000         1.330578         1.369422         Cov. Mat.           na               na               na               na               na
 ippkx190             regul_kp1      2.700000         1.965419        0.7345807         Cov. Mat.           na               na               na               na               na
 ippkx191             regul_kp1      2.700000         1.621579         1.078421         Cov. Mat.           na               na               na               na               na
 ippkx192             regul_kp1      1.700000         1.987138       -0.2871384         Cov. Mat.           na               na               na               na               na
 ippkx193             regul_kp1      1.000000         3.024768        -2.024768         Cov. Mat.           na               na               na               na               na
 ippkx194             regul_kp1    -4.9163148E-16    0.3379161       -0.3379161         Cov. Mat.           na               na               na               na               na
 ippkx195             regul_kp1      1.000000         1.356443       -0.3564433         Cov. Mat.           na               na               na               na               na
 ippkx196             regul_kp1      1.000000         1.086863       -8.6863249E-02     Cov. Mat.           na               na               na               na               na
 ippkx197             regul_kp1      1.000000         2.855505        -1.855505         Cov. Mat.           na               na               na               na               na
 ippkx198             regul_kp1      1.000000         2.124640        -1.124640         Cov. Mat.           na               na               na               na               na
 ippkx199             regul_kp1      1.000000        0.3903003        0.6096997         Cov. Mat.           na               na               na               na               na
 ippkx200             regul_kp1     2.3623681E-15     1.158482        -1.158482         Cov. Mat.           na               na               na               na               na
 ippkx201             regul_kp1      1.700000        0.1179875         1.582013         Cov. Mat.           na               na               na               na               na
 ippkx202             regul_kp1      1.700000         1.406343        0.2936567         Cov. Mat.           na               na               na               na               na
 ippkx203             regul_kp1     -1.000000        -1.141142        0.1411420         Cov. Mat.           na               na               na               na               na
 ippkx204             regul_kp1      1.000000        0.7122498        0.2877502         Cov. Mat.           na               na               na               na               na
 ippkx205             regul_kp1      1.700000        1.7791023E-02     1.682209         Cov. Mat.           na               na               na               na               na
 ippkx206             regul_kp1      1.700000        0.1377284         1.562272         Cov. Mat.           na               na               na               na               na
 ippkx207             regul_kp1      1.700000         1.994038       -0.2940379         Cov. Mat.           na               na               na               na               na
 ippkx208             regul_kp1      1.700000        6.7231377E-16     1.700000         Cov. Mat.           na               na               na               na               na
 ippkx209             regul_kp1      1.700000         2.392354       -0.6923545         Cov. Mat.           na               na               na               na               na
 ippkx210             regul_kp1      1.700000        0.9681964        0.7318036         Cov. Mat.           na               na               na               na               na
 ippkx211             regul_kp1      1.700000         3.061804        -1.361804         Cov. Mat.           na               na               na               na               na
 ippkx212             regul_kp1      1.700000        0.4105419         1.289458         Cov. Mat.           na               na               na               na               na
 ippkx213             regul_kp1      1.700000        0.3733457         1.326654         Cov. Mat.           na               na               na               na               na
 ippkx214             regul_kp1      1.700000         2.213749       -0.5137493         Cov. Mat.           na               na               na               na               na
 ippkx215             regul_kp1      1.700000         2.796681        -1.096681         Cov. Mat.           na               na               na               na               na
 ippkx216             regul_kp1     -2.000000        -1.229805       -0.7701946         Cov. Mat.           na               na               na               na               na
 ippkx217             regul_kp1      1.000000         2.698970        -1.698970         Cov. Mat.           na               na               na               na               na
 ippkx218             regul_kp1      1.000000         2.615266        -1.615266         Cov. Mat.           na               na               na               na               na
 ippkx219             regul_kp1      1.700000         2.698970       -0.9989700         Cov. Mat.           na               na               na               na               na
 ippkx220             regul_kp1     -1.000000       -0.7948059       -0.2051941         Cov. Mat.           na               na               na               na               na
 ippkx221             regul_kp1    -4.2107702E-16    0.7242025       -0.7242025         Cov. Mat.           na               na               na               na               na
 ippkx222             regul_kp1      1.700000         1.302253        0.3977475         Cov. Mat.           na               na               na               na               na
 ippkx223             regul_kp1      2.700000         1.884849        0.8151512         Cov. Mat.           na               na               na               na               na
 ippkx224             regul_kp1      2.700000         2.312837        0.3871628         Cov. Mat.           na               na               na               na               na
 ippkx225             regul_kp1      2.700000         1.416103         1.283897         Cov. Mat.           na               na               na               na               na
 ippkx226             regul_kp1      1.700000        8.8675811E-15     1.700000         Cov. Mat.           na               na               na               na               na
 ippkx227             regul_kp1      1.000000        0.9739036        2.6096377E-02     Cov. Mat.           na               na               na               na               na
 ippkx228             regul_kp1      1.000000         1.352725       -0.3527250         Cov. Mat.           na               na               na               na               na
 ippkx229             regul_kp2      2.700000         1.457917         1.242083         Cov. Mat.           na               na               na               na               na
 ippkx230             regul_kp2      2.700000         1.090643         1.609357         Cov. Mat.           na               na               na               na               na
 ippkx231             regul_kp2      2.700000         1.692374         1.007626         Cov. Mat.           na               na               na               na               na
 ippkx232             regul_kp2      2.700000         1.549127         1.150873         Cov. Mat.           na               na               na               na               na
 ippkx233             regul_kp2      2.700000         2.855711       -0.1557112         Cov. Mat.           na               na               na               na               na
 ippkx234             regul_kp1      1.700000         1.812157       -0.1121569         Cov. Mat.           na               na               na               na               na
 ippkx235             regul_kp2      2.700000         1.513341         1.186659         Cov. Mat.           na               na               na               na               na
 ippkz1               regul_kz1      1.000000         1.726886       -0.7268860         Cov. Mat.           na               na               na               na               na
 ippkz2               regul_kz1      1.000000        0.8903228        0.1096772         Cov. Mat.           na               na               na               na               na
 ippkz3               regul_kz1      1.000000        5.7444337E-02    0.9425557         Cov. Mat.           na               na               na               na               na
 ippkz4               regul_kz1      1.000000         1.468063       -0.4680626         Cov. Mat.           na               na               na               na               na
 ippkz5               regul_kz1      1.000000         1.022698       -2.2697608E-02     Cov. Mat.           na               na               na               na               na
 ippkz6               regul_kz1      1.000000        0.3201344        0.6798656         Cov. Mat.           na               na               na               na               na
 ippkz7               regul_kz1      1.000000         1.931235       -0.9312355         Cov. Mat.           na               na               na               na               na
 ippkz8               regul_kz1      1.000000         1.531690       -0.5316901         Cov. Mat.           na               na               na               na               na
 ippkz9               regul_kz1      1.000000         1.131401       -0.1314015         Cov. Mat.           na               na               na               na               na
 ippkz10              regul_kz1      1.000000        0.6464632        0.3535368         Cov. Mat.           na               na               na               na               na
 ippkz11              regul_kz1      1.000000         2.672704        -1.672704         Cov. Mat.           na               na               na               na               na
 ippkz12              regul_kz1      1.000000         1.470373       -0.4703731         Cov. Mat.           na               na               na               na               na
 ippkz13              regul_kz1      1.000000        0.8205072        0.1794928         Cov. Mat.           na               na               na               na               na
 ippkz14              regul_kz1      1.000000        0.4082410        0.5917590         Cov. Mat.           na               na               na               na               na
 ippkz15              regul_kz1      1.000000         1.387451       -0.3874505         Cov. Mat.           na               na               na               na               na
 ippkz16              regul_kz1      1.000000         1.905424       -0.9054241         Cov. Mat.           na               na               na               na               na
 ippkz17              regul_kz1      1.000000         1.456490       -0.4564898         Cov. Mat.           na               na               na               na               na
 ippkz18              regul_kz1      1.000000         1.131019       -0.1310194         Cov. Mat.           na               na               na               na               na
 ippkz19              regul_kz1      1.000000         1.432142       -0.4321417         Cov. Mat.           na               na               na               na               na
 ippkz20              regul_kz1      1.000000        0.8336778        0.1663222         Cov. Mat.           na               na               na               na               na
 ippkz21              regul_kz1      1.000000         1.396338       -0.3963382         Cov. Mat.           na               na               na               na               na
 ippkz22              regul_kz1      1.000000         1.456080       -0.4560802         Cov. Mat.           na               na               na               na               na
 ippkz23              regul_kz1      1.000000         1.512874       -0.5128742         Cov. Mat.           na               na               na               na               na
 ippkz24              regul_kz1      1.000000         1.578941       -0.5789413         Cov. Mat.           na               na               na               na               na
 ippkz25              regul_kz1      1.000000        0.1257860        0.8742140         Cov. Mat.           na               na               na               na               na
 ippkz26              regul_kz1      1.000000        3.1043099E-02    0.9689569         Cov. Mat.           na               na               na               na               na
 ippkz27              regul_kz1      1.000000         1.805009       -0.8050085         Cov. Mat.           na               na               na               na               na
 ippkz28              regul_kz1      1.000000        0.2025177        0.7974823         Cov. Mat.           na               na               na               na               na
 ippkz29              regul_kz1      1.000000        0.8864690        0.1135310         Cov. Mat.           na               na               na               na               na
 ippkz30              regul_kz1      1.000000        0.9783204        2.1679627E-02     Cov. Mat.           na               na               na               na               na
 ippkz31              regul_kz1      1.000000         1.561145       -0.5611454         Cov. Mat.           na               na               na               na               na
 ippkz32              regul_kz1      1.000000        0.7926114        0.2073886         Cov. Mat.           na               na               na               na               na
 ippkz33              regul_kz1      1.000000         1.157719       -0.1577193         Cov. Mat.           na               na               na               na               na
 ippkz34              regul_kz1      1.000000        0.4534160        0.5465840         Cov. Mat.           na               na               na               na               na
 ippkz35              regul_kz1      1.000000        0.4013643        0.5986357         Cov. Mat.           na               na               na               na               na
 ippkz36              regul_kz1      1.000000        0.9797384        2.0261561E-02     Cov. Mat.           na               na               na               na               na
 ippkz37              regul_kz1      1.000000        0.6955806        0.3044194         Cov. Mat.           na               na               na               na               na
 ippkz38              regul_kz1      1.000000        0.3375076        0.6624924         Cov. Mat.           na               na               na               na               na
 ippkz39              regul_kz1      1.000000         1.043576       -4.3575639E-02     Cov. Mat.           na               na               na               na               na
 ippkz40              regul_kz1      1.000000         2.072686        -1.072686         Cov. Mat.           na               na               na               na               na
 ippkz41              regul_kz1      1.000000        0.9441865        5.5813489E-02     Cov. Mat.           na               na               na               na               na
 ippkz42              regul_kz1      1.000000         1.304377       -0.3043774         Cov. Mat.           na               na               na               na               na
 ippkz43              regul_kz1      1.000000        0.8963308        0.1036692         Cov. Mat.           na               na               na               na               na
 ippkz44              regul_kz1      1.000000         1.633105       -0.6331048         Cov. Mat.           na               na               na               na               na
 ippkz45              regul_kz1      1.000000         1.345116       -0.3451160         Cov. Mat.           na               na               na               na               na
 ippkz46              regul_kz1      1.000000         1.137771       -0.1377714         Cov. Mat.           na               na               na               na               na
 ippkz47              regul_kz1      1.000000        0.9888509        1.1149129E-02     Cov. Mat.           na               na               na               na               na
 ippkz48              regul_kz1      1.000000         1.828822       -0.8288217         Cov. Mat.           na               na               na               na               na
 ippkz49              regul_kz1      1.000000         1.311256       -0.3112555         Cov. Mat.           na               na               na               na               na
 ippkz50              regul_kz1      1.000000         1.270859       -0.2708586         Cov. Mat.           na               na               na               na               na
 ippkz51              regul_kz1      1.000000        0.5385262        0.4614738         Cov. Mat.           na               na               na               na               na
 ippkz52              regul_kz1      1.000000        0.5767648        0.4232352         Cov. Mat.           na               na               na               na               na
 ippkz53              regul_kz1      1.000000         2.014013        -1.014013         Cov. Mat.           na               na               na               na               na
 ippkz54              regul_kz1      1.000000         1.321601       -0.3216011         Cov. Mat.           na               na               na               na               na
 ippkz55              regul_kz1      1.000000         1.451427       -0.4514273         Cov. Mat.           na               na               na               na               na
 ippkz56              regul_kz1      1.000000        0.8619354        0.1380646         Cov. Mat.           na               na               na               na               na
 ippkz57              regul_kz1      1.000000         1.760558       -0.7605577         Cov. Mat.           na               na               na               na               na
 ippkz58              regul_kz1      1.000000        0.6233354        0.3766646         Cov. Mat.           na               na               na               na               na
 ippkz59              regul_kz1      1.000000         1.022008       -2.2007896E-02     Cov. Mat.           na               na               na               na               na
 ippkz60              regul_kz1      1.000000        6.4076348E-16     1.000000         Cov. Mat.           na               na               na               na               na
 ippkz61              regul_kz1      1.000000         1.725297       -0.7252966         Cov. Mat.           na               na               na               na               na
 ippkz62              regul_kz1      1.000000         1.411151       -0.4111507         Cov. Mat.           na               na               na               na               na
 ippkz63              regul_kz1      1.000000         1.702710       -0.7027097         Cov. Mat.           na               na               na               na               na
 ippkz64              regul_kz1      1.000000         1.722857       -0.7228566         Cov. Mat.           na               na               na               na               na
 ippkz65              regul_kz1      1.000000         2.016167        -1.016167         Cov. Mat.           na               na               na               na               na
 ippkz66              regul_kz1      1.000000         1.725797       -0.7257973         Cov. Mat.           na               na               na               na               na
 ippkz67              regul_kz1      1.000000        0.7731786        0.2268214         Cov. Mat.           na               na               na               na               na
 ippkz68              regul_kz1      1.000000         1.967595       -0.9675951         Cov. Mat.           na               na               na               na               na
 ippkz69              regul_kz1      1.000000         1.708351       -0.7083511         Cov. Mat.           na               na               na               na               na
 ippkz70              regul_kz1      1.000000        0.5970020        0.4029980         Cov. Mat.           na               na               na               na               na
 ippkz71              regul_kz1      1.000000         1.131488       -0.1314884         Cov. Mat.           na               na               na               na               na
 ippkz72              regul_kz1      1.000000         1.515738       -0.5157378         Cov. Mat.           na               na               na               na               na
 ippkz73              regul_kz1      1.000000         1.475696       -0.4756960         Cov. Mat.           na               na               na               na               na
 ippkz74              regul_kz1      1.000000         1.591735       -0.5917349         Cov. Mat.           na               na               na               na               na
 ippkz75              regul_kz1      1.000000         1.332060       -0.3320598         Cov. Mat.           na               na               na               na               na
 ippkz76              regul_kz1      1.000000         2.485357        -1.485357         Cov. Mat.           na               na               na               na               na
 ippkz77              regul_kz1      1.000000         1.233595       -0.2335948         Cov. Mat.           na               na               na               na               na
 ippkz78              regul_kz1      1.000000        0.9606948        3.9305188E-02     Cov. Mat.           na               na               na               na               na
 ippkz79              regul_kz1      1.000000         1.095218       -9.5218185E-02     Cov. Mat.           na               na               na               na               na
 ippkz80              regul_kz1      1.000000        0.8992678        0.1007322         Cov. Mat.           na               na               na               na               na
 ippkz81              regul_kz1      1.000000        0.7370691        0.2629309         Cov. Mat.           na               na               na               na               na
 ippkz82              regul_kz1      1.000000         1.902557       -0.9025572         Cov. Mat.           na               na               na               na               na
 ippkz83              regul_kz1      1.000000         1.303666       -0.3036662         Cov. Mat.           na               na               na               na               na
 ippkz84              regul_kz1      1.000000         1.162513       -0.1625130         Cov. Mat.           na               na               na               na               na
 ippkz85              regul_kz1      1.000000        0.8721118        0.1278882         Cov. Mat.           na               na               na               na               na
 ippkz86              regul_kz1      1.000000         1.345204       -0.3452035         Cov. Mat.           na               na               na               na               na
 ippkz87              regul_kz1      1.000000         2.357282        -1.357282         Cov. Mat.           na               na               na               na               na
 ippkz88              regul_kz2      1.000000         2.998920        -1.998920         Cov. Mat.           na               na               na               na               na
 ippkz89              regul_kz2      1.000000         2.785635        -1.785635         Cov. Mat.           na               na               na               na               na
 ippkz90              regul_kz2      1.000000         1.805723       -0.8057225         Cov. Mat.           na               na               na               na               na
 ippkz91              regul_kz2      1.000000         1.737325       -0.7373250         Cov. Mat.           na               na               na               na               na
 ippkz92              regul_kz2      1.000000         2.113709        -1.113709         Cov. Mat.           na               na               na               na               na
 ippkz93              regul_kz2      1.000000         2.330362        -1.330362         Cov. Mat.           na               na               na               na               na
 ippkz94              regul_kz2      1.000000         2.325421        -1.325421         Cov. Mat.           na               na               na               na               na
 ippkz95              regul_kz2      1.000000         1.945380       -0.9453801         Cov. Mat.           na               na               na               na               na
 ippkz96              regul_kz2      1.000000         1.510904       -0.5109044         Cov. Mat.           na               na               na               na               na
 ippkz97              regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz98              regul_kz2      1.000000         1.630115       -0.6301153         Cov. Mat.           na               na               na               na               na
 ippkz99              regul_kz2      1.000000         1.281234       -0.2812343         Cov. Mat.           na               na               na               na               na
 ippkz100             regul_kz2      1.000000         2.452999        -1.452999         Cov. Mat.           na               na               na               na               na
 ippkz101             regul_kz2      1.000000         2.167916        -1.167916         Cov. Mat.           na               na               na               na               na
 ippkz102             regul_kz2      1.000000         2.338280        -1.338280         Cov. Mat.           na               na               na               na               na
 ippkz103             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz104             regul_kz2      1.000000         2.622067        -1.622067         Cov. Mat.           na               na               na               na               na
 ippkz105             regul_kz2      1.000000         2.887897        -1.887897         Cov. Mat.           na               na               na               na               na
 ippkz106             regul_kz2      1.000000         2.585824        -1.585824         Cov. Mat.           na               na               na               na               na
 ippkz107             regul_kz2      1.000000         2.467109        -1.467109         Cov. Mat.           na               na               na               na               na
 ippkz108             regul_kz2      1.000000         2.566113        -1.566113         Cov. Mat.           na               na               na               na               na
 ippkz109             regul_kz2      1.000000         2.580219        -1.580219         Cov. Mat.           na               na               na               na               na
 ippkz110             regul_kz2      1.000000        0.6395251        0.3604749         Cov. Mat.           na               na               na               na               na
 ippkz111             regul_kz2      1.000000         1.069545       -6.9545264E-02     Cov. Mat.           na               na               na               na               na
 ippkz112             regul_kz2      1.000000         1.803100       -0.8031002         Cov. Mat.           na               na               na               na               na
 ippkz113             regul_kz2      1.000000         1.908741       -0.9087415         Cov. Mat.           na               na               na               na               na
 ippkz114             regul_kz2      1.000000         2.343207        -1.343207         Cov. Mat.           na               na               na               na               na
 ippkz115             regul_kz2      1.000000         1.822671       -0.8226710         Cov. Mat.           na               na               na               na               na
 ippkz116             regul_kz2      1.000000         2.737705        -1.737705         Cov. Mat.           na               na               na               na               na
 ippkz117             regul_kz2      1.000000         2.214958        -1.214958         Cov. Mat.           na               na               na               na               na
 ippkz118             regul_kz2      1.000000         2.783379        -1.783379         Cov. Mat.           na               na               na               na               na
 ippkz119             regul_kz2      1.000000        0.7831304        0.2168696         Cov. Mat.           na               na               na               na               na
 ippkz120             regul_kz2      1.000000        0.7797878        0.2202122         Cov. Mat.           na               na               na               na               na
 ippkz121             regul_kz2      1.000000         1.263570       -0.2635700         Cov. Mat.           na               na               na               na               na
 ippkz122             regul_kz2      1.000000         1.369737       -0.3697365         Cov. Mat.           na               na               na               na               na
 ippkz123             regul_kz2      1.000000         2.564450        -1.564450         Cov. Mat.           na               na               na               na               na
 ippkz124             regul_kz2      1.000000         1.745920       -0.7459197         Cov. Mat.           na               na               na               na               na
 ippkz125             regul_kz2      1.000000         2.988511        -1.988511         Cov. Mat.           na               na               na               na               na
 ippkz126             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz127             regul_kz2      1.000000         1.989844       -0.9898443         Cov. Mat.           na               na               na               na               na
 ippkz128             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz129             regul_kz2      1.000000         2.657144        -1.657144         Cov. Mat.           na               na               na               na               na
 ippkz130             regul_kz2      1.000000         1.337307       -0.3373075         Cov. Mat.           na               na               na               na               na
 ippkz131             regul_kz2      1.000000         1.094931       -9.4930686E-02     Cov. Mat.           na               na               na               na               na
 ippkz132             regul_kz2      1.000000         1.611697       -0.6116967         Cov. Mat.           na               na               na               na               na
 ippkz133             regul_kz2      1.000000         2.493113        -1.493113         Cov. Mat.           na               na               na               na               na
 ippkz134             regul_kz2      1.000000         2.342180        -1.342180         Cov. Mat.           na               na               na               na               na
 ippkz135             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz136             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz137             regul_kz2      1.000000         2.921480        -1.921480         Cov. Mat.           na               na               na               na               na
 ippkz138             regul_kz2      1.000000         2.663349        -1.663349         Cov. Mat.           na               na               na               na               na
 ippkz139             regul_kz2      1.000000         1.633676       -0.6336760         Cov. Mat.           na               na               na               na               na
 ippkz140             regul_kz2      1.000000         2.237833        -1.237833         Cov. Mat.           na               na               na               na               na
 ippkz141             regul_kz2      1.000000         2.090094        -1.090094         Cov. Mat.           na               na               na               na               na
 ippkz142             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz143             regul_kz2      1.000000         2.268014        -1.268014         Cov. Mat.           na               na               na               na               na
 ippkz144             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz145             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz146             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz147             regul_kz2      1.000000         2.409093        -1.409093         Cov. Mat.           na               na               na               na               na
 ippkz148             regul_kz2      1.000000         1.093453       -9.3452855E-02     Cov. Mat.           na               na               na               na               na
 ippkz149             regul_kz2      1.000000         1.875201       -0.8752009         Cov. Mat.           na               na               na               na               na
 ippkz150             regul_kz2      1.000000         2.446921        -1.446921         Cov. Mat.           na               na               na               na               na
 ippkz151             regul_kz2      1.000000         2.102915        -1.102915         Cov. Mat.           na               na               na               na               na
 ippkz152             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz153             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz154             regul_kz2      1.000000         2.782299        -1.782299         Cov. Mat.           na               na               na               na               na
 ippkz155             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz156             regul_kz2      1.000000         1.817439       -0.8174394         Cov. Mat.           na               na               na               na               na
 ippkz157             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz158             regul_kz2      1.000000         1.145245       -0.1452452         Cov. Mat.           na               na               na               na               na
 ippkz159             regul_kz2      1.000000         1.520231       -0.5202313         Cov. Mat.           na               na               na               na               na
 ippkz160             regul_kz2      1.000000         2.752556        -1.752556         Cov. Mat.           na               na               na               na               na
 ippkz161             regul_kz2      1.000000         2.744449        -1.744449         Cov. Mat.           na               na               na               na               na
 ippkz162             regul_kz2      1.000000         2.635896        -1.635896         Cov. Mat.           na               na               na               na               na
 ippkz163             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz164             regul_kz2      1.000000         3.000000        -2.000000         Cov. Mat.           na               na               na               na               na
 ippkz165             regul_kz2      1.000000         2.449432        -1.449432         Cov. Mat.           na               na               na               na               na
 ippkz166             regul_kz2      1.000000         1.849414       -0.8494144         Cov. Mat.           na               na               na               na               na
 ippkz167             regul_kz2      1.000000         2.230206        -1.230206         Cov. Mat.           na               na               na               na               na
 ippkz168             regul_kz2      1.000000         2.102273        -1.102273         Cov. Mat.           na               na               na               na               na
 ippkz169             regul_kz2      1.000000         2.952858        -1.952858         Cov. Mat.           na               na               na               na               na
 ippkz170             regul_kz2      1.000000         1.936775       -0.9367752         Cov. Mat.           na               na               na               na               na
 ippkz171             regul_kz2      1.000000         2.519319        -1.519319         Cov. Mat.           na               na               na               na               na
 ippkz172             regul_kz2      1.000000         2.276340        -1.276340         Cov. Mat.           na               na               na               na               na
 ippkz173             regul_kz2      1.000000         2.406560        -1.406560         Cov. Mat.           na               na               na               na               na
 ippkz174             regul_kz2      1.000000         2.684615        -1.684615         Cov. Mat.           na               na               na               na               na
 ippkz175             regul_kz2      1.000000         1.885688       -0.8856885         Cov. Mat.           na               na               na               na               na
 ippkz176             regul_kz2      1.000000         1.928641       -0.9286406         Cov. Mat.           na               na               na               na               na
 ippkz177             regul_kz2      1.000000         1.606563       -0.6065632         Cov. Mat.           na               na               na               na               na
 ippkz178             regul_kz1      1.000000         2.027529        -1.027529         Cov. Mat.           na               na               na               na               na
 ippkz179             regul_kz1      1.000000        0.7860493        0.2139507         Cov. Mat.           na               na               na               na               na
 ippkz180             regul_kz1      1.000000         1.067094       -6.7094301E-02     Cov. Mat.           na               na               na               na               na
 ippkz181             regul_kz1      1.000000         1.036114       -3.6113663E-02     Cov. Mat.           na               na               na               na               na
 ippkz182             regul_kz1      1.000000        0.2282792        0.7717208         Cov. Mat.           na               na               na               na               na
 ippkz183             regul_kz1      1.000000         2.113385        -1.113385         Cov. Mat.           na               na               na               na               na
 ippkz184             regul_kz1      1.000000         1.867113       -0.8671131         Cov. Mat.           na               na               na               na               na
 ippkz185             regul_kz1      1.000000         1.144177       -0.1441771         Cov. Mat.           na               na               na               na               na
 ippkz186             regul_kz1      1.000000         1.965143       -0.9651429         Cov. Mat.           na               na               na               na               na
 ippkz187             regul_kz2      1.000000         2.630401        -1.630401         Cov. Mat.           na               na               na               na               na
 ippss1               regul_ppss    -6.000000        -6.952884        0.9528839        5.0000000E-02   -0.3000000       -0.3476442        4.7644196E-02        na               na       
 ippss2               regul_ppss    -6.000000        -4.408157        -1.591843        5.0000000E-02   -0.3000000       -0.2204079       -7.9592133E-02        na               na       
 ippss3               regul_ppss    -6.000000        -6.020249        2.0248610E-02    5.0000000E-02   -0.3000000       -0.3010124        1.0124305E-03        na               na       
 ippss4               regul_ppss    -6.000000        -5.732049       -0.2679507        5.0000000E-02   -0.3000000       -0.2866025       -1.3397534E-02        na               na       
 ippss5               regul_ppss    -6.000000        -6.892100        0.8921001        5.0000000E-02   -0.3000000       -0.3446050        4.4605003E-02        na               na       
 ippss6               regul_ppss    -6.000000        -5.698975       -0.3010252        5.0000000E-02   -0.3000000       -0.2849487       -1.5051260E-02        na               na       
 ippss7               regul_ppss    -6.000000        -6.866642        0.8666423        5.0000000E-02   -0.3000000       -0.3433321        4.3332116E-02        na               na       
 ippss8               regul_ppss    -6.000000        -5.685132       -0.3148679        5.0000000E-02   -0.3000000       -0.2842566       -1.5743393E-02        na               na       
 ippss9               regul_ppss    -6.000000        -5.877763       -0.1222370        5.0000000E-02   -0.3000000       -0.2938882       -6.1118493E-03        na               na       
 ippss10              regul_ppss    -6.000000        -5.972287       -2.7712999E-02    5.0000000E-02   -0.3000000       -0.2986144       -1.3856499E-03        na               na       
 ippss11              regul_ppss    -6.000000        -5.391194       -0.6088060        5.0000000E-02   -0.3000000       -0.2695597       -3.0440298E-02        na               na       
 ippss12              regul_ppss    -6.000000        -5.419032       -0.5809675        5.0000000E-02   -0.3000000       -0.2709516       -2.9048376E-02        na               na       
 ippss13              regul_ppss    -6.000000        -5.578384       -0.4216159        5.0000000E-02   -0.3000000       -0.2789192       -2.1080797E-02        na               na       
 ippss14              regul_ppss    -6.000000        -5.506096       -0.4939039        5.0000000E-02   -0.3000000       -0.2753048       -2.4695194E-02        na               na       
 ippss15              regul_ppss    -6.000000        -5.897988       -0.1020124        5.0000000E-02   -0.3000000       -0.2948994       -5.1006190E-03        na               na       
 ippss16              regul_ppss    -6.000000        -6.600539        0.6005392        5.0000000E-02   -0.3000000       -0.3300270        3.0026962E-02        na               na       
 ippss17              regul_ppss    -6.000000        -6.527403        0.5274028        5.0000000E-02   -0.3000000       -0.3263701        2.6370138E-02        na               na       
 ippss18              regul_ppss    -6.000000        -5.175674       -0.8243256        5.0000000E-02   -0.3000000       -0.2587837       -4.1216281E-02        na               na       
 ippss19              regul_ppss    -6.000000        -6.140717        0.1407172        5.0000000E-02   -0.3000000       -0.3070359        7.0358598E-03        na               na       
 ippss20              regul_ppss    -6.000000        -4.570704        -1.429296        5.0000000E-02   -0.3000000       -0.2285352       -7.1464781E-02        na               na       
 ippss21              regul_ppss    -6.000000        -6.096191        9.6190554E-02    5.0000000E-02   -0.3000000       -0.3048095        4.8095277E-03        na               na       
 ippss22              regul_ppss    -6.000000        -5.249654       -0.7503456        5.0000000E-02   -0.3000000       -0.2624827       -3.7517278E-02        na               na       
 ippss23              regul_ppss    -6.000000        -4.607818        -1.392182        5.0000000E-02   -0.3000000       -0.2303909       -6.9609112E-02        na               na       
 ippss24              regul_ppss    -6.000000        -5.122188       -0.8778118        5.0000000E-02   -0.3000000       -0.2561094       -4.3890592E-02        na               na       
 ippss25              regul_ppss    -6.000000        -6.998854        0.9988537        5.0000000E-02   -0.3000000       -0.3499427        4.9942684E-02        na               na       
 ippss26              regul_ppss    -6.000000        -4.756209        -1.243791        5.0000000E-02   -0.3000000       -0.2378105       -6.2189542E-02        na               na       
 ippss27              regul_ppss    -6.000000        -4.618482        -1.381518        5.0000000E-02   -0.3000000       -0.2309241       -6.9075881E-02        na               na       
 ippss28              regul_ppss    -6.000000        -6.517190        0.5171904        5.0000000E-02   -0.3000000       -0.3258595        2.5859520E-02        na               na       
 ippss29              regul_ppss    -6.000000        -5.901388       -9.8611992E-02    5.0000000E-02   -0.3000000       -0.2950694       -4.9305996E-03        na               na       
 ippss30              regul_ppss    -6.000000        -7.000000         1.000000        5.0000000E-02   -0.3000000       -0.3500000        5.0000000E-02        na               na       
 ippss31              regul_ppss    -6.000000        -6.167684        0.1676837        5.0000000E-02   -0.3000000       -0.3083842        8.3841873E-03        na               na       
 ippss32              regul_ppss    -6.000000        -5.203936       -0.7960641        5.0000000E-02   -0.3000000       -0.2601968       -3.9803204E-02        na               na       
 ippss33              regul_ppss    -6.000000        -4.939596        -1.060404        5.0000000E-02   -0.3000000       -0.2469798       -5.3020189E-02        na               na       
 ippss34              regul_ppss    -6.000000        -6.579315        0.5793150        5.0000000E-02   -0.3000000       -0.3289657        2.8965748E-02        na               na       
 ippss35              regul_ppss    -6.000000        -6.658741        0.6587411        5.0000000E-02   -0.3000000       -0.3329371        3.2937056E-02        na               na       
 ippss36              regul_ppss    -6.000000        -5.654857       -0.3451432        5.0000000E-02   -0.3000000       -0.2827428       -1.7257162E-02        na               na       
 ippss37              regul_ppss    -6.000000        -6.520225        0.5202254        5.0000000E-02   -0.3000000       -0.3260113        2.6011269E-02        na               na       
 ippss38              regul_ppss    -6.000000        -6.293210        0.2932095        5.0000000E-02   -0.3000000       -0.3146605        1.4660475E-02        na               na       
 ippss39              regul_ppss    -6.000000        -6.164459        0.1644594        5.0000000E-02   -0.3000000       -0.3082230        8.2229686E-03        na               na       
 ippss40              regul_ppss    -6.000000        -6.174333        0.1743327        5.0000000E-02   -0.3000000       -0.3087166        8.7166365E-03        na               na       
 ippss41              regul_ppss    -6.000000        -5.475900       -0.5240997        5.0000000E-02   -0.3000000       -0.2737950       -2.6204985E-02        na               na       
 ippss42              regul_ppss    -6.000000        -5.867327       -0.1326732        5.0000000E-02   -0.3000000       -0.2933663       -6.6336581E-03        na               na       
 ippss43              regul_ppss    -6.000000        -6.208885        0.2088845        5.0000000E-02   -0.3000000       -0.3104442        1.0444225E-02        na               na       
 ippss44              regul_ppss    -6.000000        -6.792313        0.7923130        5.0000000E-02   -0.3000000       -0.3396156        3.9615650E-02        na               na       
 ippss45              regul_ppss    -6.000000        -6.043449        4.3449376E-02    5.0000000E-02   -0.3000000       -0.3021725        2.1724688E-03        na               na       
 ippss46              regul_ppss    -6.000000        -6.342590        0.3425903        5.0000000E-02   -0.3000000       -0.3171295        1.7129517E-02        na               na       
 ippss47              regul_ppss    -6.000000        -5.786110       -0.2138901        5.0000000E-02   -0.3000000       -0.2893055       -1.0694507E-02        na               na       
 ippss48              regul_ppss    -6.000000        -5.540865       -0.4591353        5.0000000E-02   -0.3000000       -0.2770432       -2.2956766E-02        na               na       
 ippss49              regul_ppss    -6.000000        -5.908183       -9.1817335E-02    5.0000000E-02   -0.3000000       -0.2954091       -4.5908667E-03        na               na       
 ippss50              regul_ppss    -6.000000        -5.482600       -0.5174001        5.0000000E-02   -0.3000000       -0.2741300       -2.5870004E-02        na               na       
 ippss51              regul_ppss    -6.000000        -6.539733        0.5397333        5.0000000E-02   -0.3000000       -0.3269867        2.6986664E-02        na               na       
 ippss52              regul_ppss    -6.000000        -5.847220       -0.1527804        5.0000000E-02   -0.3000000       -0.2923610       -7.6390189E-03        na               na       
 ippss53              regul_ppss    -6.000000        -6.275768        0.2757683        5.0000000E-02   -0.3000000       -0.3137884        1.3788415E-02        na               na       
 ippss54              regul_ppss    -6.000000        -5.224547       -0.7754528        5.0000000E-02   -0.3000000       -0.2612274       -3.8772640E-02        na               na       
 ippss55              regul_ppss    -6.000000        -6.626092        0.6260918        5.0000000E-02   -0.3000000       -0.3313046        3.1304591E-02        na               na       
 ippss56              regul_ppss    -6.000000        -5.874027       -0.1259733        5.0000000E-02   -0.3000000       -0.2937013       -6.2986659E-03        na               na       
 ippss57              regul_ppss    -6.000000        -4.575349        -1.424651        5.0000000E-02   -0.3000000       -0.2287675       -7.1232541E-02        na               na       
 ippss58              regul_ppss    -6.000000        -6.754876        0.7548765        5.0000000E-02   -0.3000000       -0.3377438        3.7743825E-02        na               na       
 ippss59              regul_ppss    -6.000000        -6.121157        0.1211573        5.0000000E-02   -0.3000000       -0.3060579        6.0578659E-03        na               na       
 ippss60              regul_ppss    -6.000000        -5.858905       -0.1410945        5.0000000E-02   -0.3000000       -0.2929453       -7.0547271E-03        na               na       
 ippss61              regul_ppss    -6.000000        -6.412312        0.4123120        5.0000000E-02   -0.3000000       -0.3206156        2.0615601E-02        na               na       
 ippss62              regul_ppss    -6.000000        -5.897408       -0.1025919        5.0000000E-02   -0.3000000       -0.2948704       -5.1295962E-03        na               na       
 ippss63              regul_ppss    -6.000000        -5.941594       -5.8406207E-02    5.0000000E-02   -0.3000000       -0.2970797       -2.9203104E-03        na               na       
 ippss64              regul_ppss    -6.000000        -6.555997        0.5559968        5.0000000E-02   -0.3000000       -0.3277998        2.7799842E-02        na               na       
 ippss65              regul_ppss    -6.000000        -6.176588        0.1765875        5.0000000E-02   -0.3000000       -0.3088294        8.8293768E-03        na               na       
 ippss66              regul_ppss    -6.000000        -6.049782        4.9781574E-02    5.0000000E-02   -0.3000000       -0.3024891        2.4890787E-03        na               na       
 ippss67              regul_ppss    -6.000000        -5.442690       -0.5573095        5.0000000E-02   -0.3000000       -0.2721345       -2.7865476E-02        na               na       
 ippss68              regul_ppss    -6.000000        -6.899353        0.8993528        5.0000000E-02   -0.3000000       -0.3449676        4.4967640E-02        na               na       
 ippss69              regul_ppss    -6.000000        -5.405306       -0.5946938        5.0000000E-02   -0.3000000       -0.2702653       -2.9734689E-02        na               na       
 ippss70              regul_ppss    -6.000000        -5.574787       -0.4252127        5.0000000E-02   -0.3000000       -0.2787394       -2.1260637E-02        na               na       
 ippss71              regul_ppss    -6.000000        -6.272509        0.2725088        5.0000000E-02   -0.3000000       -0.3136254        1.3625441E-02        na               na       
 ippss72              regul_ppss    -6.000000        -6.559235        0.5592345        5.0000000E-02   -0.3000000       -0.3279617        2.7961726E-02        na               na       
 ippss73              regul_ppss    -6.000000        -6.662928        0.6629280        5.0000000E-02   -0.3000000       -0.3331464        3.3146400E-02        na               na       
 ippss74              regul_ppss    -6.000000        -4.320167        -1.679833        5.0000000E-02   -0.3000000       -0.2160084       -8.3991638E-02        na               na       
 ippss75              regul_ppss    -6.000000        -5.680981       -0.3190188        5.0000000E-02   -0.3000000       -0.2840491       -1.5950942E-02        na               na       
 ippss76              regul_ppss    -6.000000        -6.523496        0.5234962        5.0000000E-02   -0.3000000       -0.3261748        2.6174809E-02        na               na       
 ippss77              regul_ppss    -6.000000        -6.349641        0.3496412        5.0000000E-02   -0.3000000       -0.3174821        1.7482059E-02        na               na       
 ippss78              regul_ppss    -6.000000        -6.801330        0.8013305        5.0000000E-02   -0.3000000       -0.3400665        4.0066524E-02        na               na       
 ippss79              regul_ppss    -6.000000        -5.620237       -0.3797631        5.0000000E-02   -0.3000000       -0.2810118       -1.8988155E-02        na               na       
 ippss80              regul_ppss    -6.000000        -5.781857       -0.2181426        5.0000000E-02   -0.3000000       -0.2890929       -1.0907130E-02        na               na       
 ippss81              regul_ppss    -6.000000        -6.461840        0.4618404        5.0000000E-02   -0.3000000       -0.3230920        2.3092020E-02        na               na       
 ippss82              regul_ppss    -6.000000        -5.561019       -0.4389813        5.0000000E-02   -0.3000000       -0.2780509       -2.1949063E-02        na               na       
 ippss83              regul_ppss    -6.000000        -6.349843        0.3498435        5.0000000E-02   -0.3000000       -0.3174922        1.7492173E-02        na               na       
 ippss84              regul_ppss    -6.000000        -5.811635       -0.1883652        5.0000000E-02   -0.3000000       -0.2905817       -9.4182611E-03        na               na       
 ippss85              regul_ppss    -6.000000        -5.618127       -0.3818728        5.0000000E-02   -0.3000000       -0.2809064       -1.9093640E-02        na               na       
 ippss86              regul_ppss    -6.000000        -5.519522       -0.4804775        5.0000000E-02   -0.3000000       -0.2759761       -2.4023877E-02        na               na       
 ippss87              regul_ppss    -6.000000        -6.999604        0.9996045        5.0000000E-02   -0.3000000       -0.3499802        4.9980225E-02        na               na       
 ippss88              regul_ppss    -6.000000        -6.326524        0.3265244        5.0000000E-02   -0.3000000       -0.3163262        1.6326221E-02        na               na       
 ippss89              regul_ppss    -6.000000        -5.941207       -5.8792628E-02    5.0000000E-02   -0.3000000       -0.2970604       -2.9396314E-03        na               na       
 ippss90              regul_ppss    -6.000000        -5.476592       -0.5234083        5.0000000E-02   -0.3000000       -0.2738296       -2.6170414E-02        na               na       
 ippss91              regul_ppss    -6.000000        -5.588623       -0.4113771        5.0000000E-02   -0.3000000       -0.2794311       -2.0568853E-02        na               na       
 ippss92              regul_ppss    -6.000000        -5.789151       -0.2108486        5.0000000E-02   -0.3000000       -0.2894576       -1.0542430E-02        na               na       
 ippss93              regul_ppss    -6.000000        -4.927934        -1.072066        5.0000000E-02   -0.3000000       -0.2463967       -5.3603294E-02        na               na       
 ippss94              regul_ppss    -6.000000        -6.524059        0.5240585        5.0000000E-02   -0.3000000       -0.3262029        2.6202927E-02        na               na       
 ippss95              regul_ppss    -6.000000        -4.835635        -1.164365        5.0000000E-02   -0.3000000       -0.2417817       -5.8218263E-02        na               na       
 ippss96              regul_ppss    -6.000000        -5.350594       -0.6494063        5.0000000E-02   -0.3000000       -0.2675297       -3.2470317E-02        na               na       
 ippss97              regul_ppss    -6.000000        -6.284609        0.2846091        5.0000000E-02   -0.3000000       -0.3142305        1.4230457E-02        na               na       
 ippss98              regul_ppss    -6.000000        -5.448583       -0.5514167        5.0000000E-02   -0.3000000       -0.2724292       -2.7570835E-02        na               na       
 ippss99              regul_ppss    -6.000000        -6.294727        0.2947268        5.0000000E-02   -0.3000000       -0.3147363        1.4736342E-02        na               na       
 ippss100             regul_ppss    -6.000000        -6.517793        0.5177933        5.0000000E-02   -0.3000000       -0.3258897        2.5889663E-02        na               na       
 ippss101             regul_ppss    -6.000000        -6.792725        0.7927251        5.0000000E-02   -0.3000000       -0.3396363        3.9636255E-02        na               na       
 ippss102             regul_ppss    -6.000000        -6.226165        0.2261647        5.0000000E-02   -0.3000000       -0.3113082        1.1308233E-02        na               na       
 ippss103             regul_ppss    -6.000000        -5.707545       -0.2924550        5.0000000E-02   -0.3000000       -0.2853773       -1.4622749E-02        na               na       
 ippss104             regul_ppss    -6.000000        -6.645842        0.6458420        5.0000000E-02   -0.3000000       -0.3322921        3.2292098E-02        na               na       
 ippss105             regul_ppss    -6.000000        -6.489480        0.4894804        5.0000000E-02   -0.3000000       -0.3244740        2.4474018E-02        na               na       
 ippss106             regul_ppss    -6.000000        -6.387516        0.3875159        5.0000000E-02   -0.3000000       -0.3193758        1.9375796E-02        na               na       
 ippss107             regul_ppss    -6.000000        -5.733983       -0.2660169        5.0000000E-02   -0.3000000       -0.2866992       -1.3300844E-02        na               na       
 ippss108             regul_ppss    -6.000000        -5.578412       -0.4215879        5.0000000E-02   -0.3000000       -0.2789206       -2.1079396E-02        na               na       
 ippss109             regul_ppss    -6.000000        -6.139458        0.1394577        5.0000000E-02   -0.3000000       -0.3069729        6.9728872E-03        na               na       
 ippss110             regul_ppss    -6.000000        -6.299648        0.2996480        5.0000000E-02   -0.3000000       -0.3149824        1.4982399E-02        na               na       
 ippss111             regul_ppss    -6.000000        -5.656963       -0.3430373        5.0000000E-02   -0.3000000       -0.2828481       -1.7151863E-02        na               na       
 ippss112             regul_ppss    -6.000000        -5.905695       -9.4304576E-02    5.0000000E-02   -0.3000000       -0.2952848       -4.7152288E-03        na               na       
 ippss113             regul_ppss    -6.000000        -6.999791        0.9997915        5.0000000E-02   -0.3000000       -0.3499896        4.9989573E-02        na               na       
 ippss114             regul_ppss    -6.000000        -5.498618       -0.5013817        5.0000000E-02   -0.3000000       -0.2749309       -2.5069086E-02        na               na       
 ippss115             regul_ppss    -6.000000        -6.589332        0.5893323        5.0000000E-02   -0.3000000       -0.3294666        2.9466616E-02        na               na       
 ippss116             regul_ppss    -6.000000        -5.042735       -0.9572649        5.0000000E-02   -0.3000000       -0.2521368       -4.7863243E-02        na               na       
 ippss117             regul_ppss    -6.000000        -5.895259       -0.1047408        5.0000000E-02   -0.3000000       -0.2947630       -5.2370393E-03        na               na       
 ippss118             regul_ppss    -6.000000        -5.098142       -0.9018576        5.0000000E-02   -0.3000000       -0.2549071       -4.5092881E-02        na               na       
 ippss119             regul_ppss    -6.000000        -5.701909       -0.2980913        5.0000000E-02   -0.3000000       -0.2850954       -1.4904566E-02        na               na       
 ippss120             regul_ppss    -6.000000        -4.438647        -1.561353        5.0000000E-02   -0.3000000       -0.2219324       -7.8067648E-02        na               na       
 ippss121             regul_ppss    -6.000000        -5.974435       -2.5564726E-02    5.0000000E-02   -0.3000000       -0.2987218       -1.2782363E-03        na               na       
 ippss122             regul_ppss    -6.000000        -6.452428        0.4524280        5.0000000E-02   -0.3000000       -0.3226214        2.2621401E-02        na               na       
 ippss123             regul_ppss    -6.000000        -6.427366        0.4273662        5.0000000E-02   -0.3000000       -0.3213683        2.1368309E-02        na               na       
 ippss124             regul_ppss    -6.000000        -5.720629       -0.2793711        5.0000000E-02   -0.3000000       -0.2860314       -1.3968554E-02        na               na       
 ippss125             regul_ppss    -6.000000        -4.474410        -1.525590        5.0000000E-02   -0.3000000       -0.2237205       -7.6279507E-02        na               na       
 ippss126             regul_ppss    -6.000000        -5.874894       -0.1251060        5.0000000E-02   -0.3000000       -0.2937447       -6.2552992E-03        na               na       
 ippss127             regul_ppss    -6.000000        -5.589405       -0.4105946        5.0000000E-02   -0.3000000       -0.2794703       -2.0529728E-02        na               na       
 ippss128             regul_ppss    -6.000000        -6.205852        0.2058516        5.0000000E-02   -0.3000000       -0.3102926        1.0292582E-02        na               na       
 ippss129             regul_ppss    -6.000000        -6.462697        0.4626968        5.0000000E-02   -0.3000000       -0.3231348        2.3134842E-02        na               na       
 ippss130             regul_ppss    -6.000000        -5.926499       -7.3501332E-02    5.0000000E-02   -0.3000000       -0.2963249       -3.6750666E-03        na               na       
 ippss131             regul_ppss    -6.000000        -5.905336       -9.4663950E-02    5.0000000E-02   -0.3000000       -0.2952668       -4.7331975E-03        na               na       
 ippss132             regul_ppss    -6.000000        -5.757405       -0.2425947        5.0000000E-02   -0.3000000       -0.2878703       -1.2129736E-02        na               na       
 ippss133             regul_ppss    -6.000000        -5.061465       -0.9385353        5.0000000E-02   -0.3000000       -0.2530732       -4.6926765E-02        na               na       
 ippss134             regul_ppss    -6.000000        -5.414746       -0.5852541        5.0000000E-02   -0.3000000       -0.2707373       -2.9262706E-02        na               na       
 ippss135             regul_ppss    -6.000000        -6.312789        0.3127885        5.0000000E-02   -0.3000000       -0.3156394        1.5639427E-02        na               na       
 ippss136             regul_ppss    -6.000000        -5.454872       -0.5451284        5.0000000E-02   -0.3000000       -0.2727436       -2.7256421E-02        na               na       
 ippss137             regul_ppss    -6.000000        -6.323169        0.3231687        5.0000000E-02   -0.3000000       -0.3161584        1.6158437E-02        na               na       
 ippss138             regul_ppss    -6.000000        -6.051844        5.1844327E-02    5.0000000E-02   -0.3000000       -0.3025922        2.5922164E-03        na               na       
 ippss139             regul_ppss    -6.000000        -5.134000       -0.8660001        5.0000000E-02   -0.3000000       -0.2567000       -4.3300003E-02        na               na       
 ippss140             regul_ppss    -6.000000        -5.046181       -0.9538189        5.0000000E-02   -0.3000000       -0.2523091       -4.7690947E-02        na               na       
 ippss141             regul_ppss    -6.000000        -5.361195       -0.6388053        5.0000000E-02   -0.3000000       -0.2680597       -3.1940266E-02        na               na       
 ippss142             regul_ppss    -6.000000        -4.922464        -1.077536        5.0000000E-02   -0.3000000       -0.2461232       -5.3876778E-02        na               na       
 ippss143             regul_ppss    -6.000000        -6.005682        5.6820157E-03    5.0000000E-02   -0.3000000       -0.3002841        2.8410079E-04        na               na       
 ippss144             regul_ppss    -6.000000        -6.969409        0.9694086        5.0000000E-02   -0.3000000       -0.3484704        4.8470431E-02        na               na       
 ippss145             regul_ppss    -6.000000        -6.043497        4.3496888E-02    5.0000000E-02   -0.3000000       -0.3021748        2.1748444E-03        na               na       
 ippss146             regul_ppss    -6.000000        -5.679212       -0.3207875        5.0000000E-02   -0.3000000       -0.2839606       -1.6039375E-02        na               na       
 ippss147             regul_ppss    -6.000000        -6.692550        0.6925498        5.0000000E-02   -0.3000000       -0.3346275        3.4627492E-02        na               na       
 ippss148             regul_ppss    -6.000000        -6.085987        8.5987027E-02    5.0000000E-02   -0.3000000       -0.3042994        4.2993513E-03        na               na       
 ippss149             regul_ppss    -6.000000        -6.228537        0.2285373        5.0000000E-02   -0.3000000       -0.3114269        1.1426867E-02        na               na       
 ippss150             regul_ppss    -6.000000        -6.338592        0.3385916        5.0000000E-02   -0.3000000       -0.3169296        1.6929579E-02        na               na       
 ippss151             regul_ppss    -6.000000        -5.867361       -0.1326389        5.0000000E-02   -0.3000000       -0.2933681       -6.6319462E-03        na               na       
 ippss152             regul_ppss    -6.000000        -6.486029        0.4860292        5.0000000E-02   -0.3000000       -0.3243015        2.4301461E-02        na               na       
 ippss153             regul_ppss    -6.000000        -5.809143       -0.1908572        5.0000000E-02   -0.3000000       -0.2904571       -9.5428620E-03        na               na       
 ippss154             regul_ppss    -6.000000        -6.670740        0.6707401        5.0000000E-02   -0.3000000       -0.3335370        3.3537007E-02        na               na       
 ippss155             regul_ppss    -6.000000        -5.360777       -0.6392232        5.0000000E-02   -0.3000000       -0.2680388       -3.1961161E-02        na               na       
 ippss156             regul_ppss    -6.000000        -6.535501        0.5355013        5.0000000E-02   -0.3000000       -0.3267751        2.6775066E-02        na               na       
 ippss157             regul_ppss    -6.000000        -6.827146        0.8271463        5.0000000E-02   -0.3000000       -0.3413573        4.1357314E-02        na               na       
 ippss158             regul_ppss    -6.000000        -5.202219       -0.7977813        5.0000000E-02   -0.3000000       -0.2601109       -3.9889066E-02        na               na       
 ippss159             regul_ppss    -6.000000        -6.704993        0.7049928        5.0000000E-02   -0.3000000       -0.3352496        3.5249640E-02        na               na       
 ippss160             regul_ppss    -6.000000        -4.746441        -1.253559        5.0000000E-02   -0.3000000       -0.2373220       -6.2677966E-02        na               na       
 ippss161             regul_ppss    -6.000000        -5.545793       -0.4542074        5.0000000E-02   -0.3000000       -0.2772896       -2.2710371E-02        na               na       
 ippss162             regul_ppss    -6.000000        -6.995654        0.9956542        5.0000000E-02   -0.3000000       -0.3497827        4.9782708E-02        na               na       
 ippss163             regul_ppss    -6.000000        -6.333840        0.3338402        5.0000000E-02   -0.3000000       -0.3166920        1.6692009E-02        na               na       
 ippss164             regul_ppss    -6.000000        -6.998411        0.9984106        5.0000000E-02   -0.3000000       -0.3499205        4.9920529E-02        na               na       
 ippss165             regul_ppss    -6.000000        -5.882546       -0.1174540        5.0000000E-02   -0.3000000       -0.2941273       -5.8726996E-03        na               na       
 ippss166             regul_ppss    -6.000000        -4.485341        -1.514659        5.0000000E-02   -0.3000000       -0.2242670       -7.5732971E-02        na               na       
 ippss167             regul_ppss    -6.000000        -5.234159       -0.7658415        5.0000000E-02   -0.3000000       -0.2617079       -3.8292074E-02        na               na       
 ippss168             regul_ppss    -6.000000        -5.480677       -0.5193232        5.0000000E-02   -0.3000000       -0.2740338       -2.5966162E-02        na               na       
 ippss169             regul_ppss    -6.000000        -5.870223       -0.1297771        5.0000000E-02   -0.3000000       -0.2935111       -6.4888564E-03        na               na       
 ippss170             regul_ppss    -6.000000        -6.273819        0.2738192        5.0000000E-02   -0.3000000       -0.3136910        1.3690958E-02        na               na       
 ippss171             regul_ppss    -6.000000        -5.991485       -8.5151354E-03    5.0000000E-02   -0.3000000       -0.2995742       -4.2575677E-04        na               na       
 ippss172             regul_ppss    -6.000000        -6.681613        0.6816132        5.0000000E-02   -0.3000000       -0.3340807        3.4080659E-02        na               na       
 ippss173             regul_ppss    -6.000000        -6.113641        0.1136408        5.0000000E-02   -0.3000000       -0.3056820        5.6820381E-03        na               na       
 ippss174             regul_ppss    -6.000000        -6.583817        0.5838166        5.0000000E-02   -0.3000000       -0.3291908        2.9190828E-02        na               na       
 ippss175             regul_ppss    -6.000000        -5.576276       -0.4237244        5.0000000E-02   -0.3000000       -0.2788138       -2.1186222E-02        na               na       
 ippss176             regul_ppss    -6.000000        -5.450069       -0.5499315        5.0000000E-02   -0.3000000       -0.2725034       -2.7496573E-02        na               na       
 ippss177             regul_ppss    -6.000000        -6.202271        0.2022711        5.0000000E-02   -0.3000000       -0.3101136        1.0113557E-02        na               na       
 ippss178             regul_ppss    -6.000000        -5.201996       -0.7980040        5.0000000E-02   -0.3000000       -0.2600998       -3.9900199E-02        na               na       
 ippss179             regul_ppss    -6.000000        -5.481644       -0.5183564        5.0000000E-02   -0.3000000       -0.2740822       -2.5917822E-02        na               na       
 ippss180             regul_ppss    -6.000000        -6.704785        0.7047847        5.0000000E-02   -0.3000000       -0.3352392        3.5239235E-02        na               na       
 ippss181             regul_ppss    -6.000000        -6.166800        0.1668003        5.0000000E-02   -0.3000000       -0.3083400        8.3400142E-03        na               na       
 ippss182             regul_ppss    -6.000000        -5.750185       -0.2498150        5.0000000E-02   -0.3000000       -0.2875092       -1.2490752E-02        na               na       
 ippss183             regul_ppss    -6.000000        -5.980093       -1.9906610E-02    5.0000000E-02   -0.3000000       -0.2990047       -9.9533051E-04        na               na       
 ippss184             regul_ppss    -6.000000        -6.327573        0.3275727        5.0000000E-02   -0.3000000       -0.3163786        1.6378634E-02        na               na       
 ippss185             regul_ppss    -6.000000        -6.234773        0.2347728        5.0000000E-02   -0.3000000       -0.3117386        1.1738641E-02        na               na       
 ippsy1               regul_ppsy    -1.000000        -1.107260        0.1072597        5.0000000E-02   -5.0000000E-02   -5.5362984E-02    5.3629843E-03        na               na       
 ippsy2               regul_ppsy    -1.000000        -1.312534        0.3125341        5.0000000E-02   -5.0000000E-02   -6.5626703E-02    1.5626703E-02        na               na       
 ippsy3               regul_ppsy    -1.000000        -1.557133        0.5571330        5.0000000E-02   -5.0000000E-02   -7.7856648E-02    2.7856648E-02        na               na       
 ippsy4               regul_ppsy    -1.000000        -1.390821        0.3908212        5.0000000E-02   -5.0000000E-02   -6.9541058E-02    1.9541058E-02        na               na       
 ippsy5               regul_ppsy    -1.000000        -1.229135        0.2291346        5.0000000E-02   -5.0000000E-02   -6.1456730E-02    1.1456730E-02        na               na       
 ippsy6               regul_ppsy    -1.000000        -1.686011        0.6860115        5.0000000E-02   -5.0000000E-02   -8.4300573E-02    3.4300573E-02        na               na       
 ippsy7               regul_ppsy    -1.000000        -1.320202        0.3202024        5.0000000E-02   -5.0000000E-02   -6.6010120E-02    1.6010120E-02        na               na       
 ippsy8               regul_ppsy    -1.000000        -1.478758        0.4787578        5.0000000E-02   -5.0000000E-02   -7.3937889E-02    2.3937889E-02        na               na       
 ippsy9               regul_ppsy    -1.000000       -0.7555922       -0.2444078        5.0000000E-02   -5.0000000E-02   -3.7779612E-02   -1.2220388E-02        na               na       
 ippsy10              regul_ppsy    -1.000000        -1.753092        0.7530916        5.0000000E-02   -5.0000000E-02   -8.7654581E-02    3.7654581E-02        na               na       
 ippsy11              regul_ppsy    -1.000000        -1.878993        0.8789934        5.0000000E-02   -5.0000000E-02   -9.3949669E-02    4.3949669E-02        na               na       
 ippsy12              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy13              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy14              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy15              regul_ppsy    -1.000000       -0.8440473       -0.1559527        5.0000000E-02   -5.0000000E-02   -4.2202364E-02   -7.7976361E-03        na               na       
 ippsy16              regul_ppsy    -1.000000       -0.7215969       -0.2784031        5.0000000E-02   -5.0000000E-02   -3.6079847E-02   -1.3920153E-02        na               na       
 ippsy17              regul_ppsy    -1.000000       -0.8661275       -0.1338725        5.0000000E-02   -5.0000000E-02   -4.3306376E-02   -6.6936242E-03        na               na       
 ippsy18              regul_ppsy    -1.000000        -1.008339        8.3388906E-03    5.0000000E-02   -5.0000000E-02   -5.0416945E-02    4.1694453E-04        na               na       
 ippsy19              regul_ppsy    -1.000000       -0.9431755       -5.6824461E-02    5.0000000E-02   -5.0000000E-02   -4.7158777E-02   -2.8412231E-03        na               na       
 ippsy20              regul_ppsy    -1.000000       -0.8050857       -0.1949143        5.0000000E-02   -5.0000000E-02   -4.0254284E-02   -9.7457160E-03        na               na       
 ippsy21              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy22              regul_ppsy    -1.000000       -0.8504724       -0.1495276        5.0000000E-02   -5.0000000E-02   -4.2523619E-02   -7.4763815E-03        na               na       
 ippsy23              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy24              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy25              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy26              regul_ppsy    -1.000000        -1.382346        0.3823459        5.0000000E-02   -5.0000000E-02   -6.9117293E-02    1.9117293E-02        na               na       
 ippsy27              regul_ppsy    -1.000000       -0.8918461       -0.1081539        5.0000000E-02   -5.0000000E-02   -4.4592306E-02   -5.4076936E-03        na               na       
 ippsy28              regul_ppsy    -1.000000        -1.000974        9.7427645E-04    5.0000000E-02   -5.0000000E-02   -5.0048714E-02    4.8713823E-05        na               na       
 ippsy29              regul_ppsy    -1.000000        -2.471964         1.471964        5.0000000E-02   -5.0000000E-02   -0.1235982        7.3598211E-02        na               na       
 ippsy30              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy31              regul_ppsy    -1.000000        -1.423920        0.4239200        5.0000000E-02   -5.0000000E-02   -7.1195998E-02    2.1195998E-02        na               na       
 ippsy32              regul_ppsy    -1.000000        -1.492946        0.4929462        5.0000000E-02   -5.0000000E-02   -7.4647308E-02    2.4647308E-02        na               na       
 ippsy33              regul_ppsy    -1.000000        -1.749358        0.7493580        5.0000000E-02   -5.0000000E-02   -8.7467899E-02    3.7467899E-02        na               na       
 ippsy34              regul_ppsy    -1.000000       -0.9722310       -2.7769019E-02    5.0000000E-02   -5.0000000E-02   -4.8611549E-02   -1.3884510E-03        na               na       
 ippsy35              regul_ppsy    -1.000000        -2.496413         1.496413        5.0000000E-02   -5.0000000E-02   -0.1248206        7.4820629E-02        na               na       
 ippsy36              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy37              regul_ppsy    -1.000000       -0.9201794       -7.9820636E-02    5.0000000E-02   -5.0000000E-02   -4.6008968E-02   -3.9910318E-03        na               na       
 ippsy38              regul_ppsy    -1.000000       -0.7329666       -0.2670334        5.0000000E-02   -5.0000000E-02   -3.6648328E-02   -1.3351672E-02        na               na       
 ippsy39              regul_ppsy    -1.000000       -0.8156010       -0.1843990        5.0000000E-02   -5.0000000E-02   -4.0780052E-02   -9.2199482E-03        na               na       
 ippsy40              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy41              regul_ppsy    -1.000000        -1.371331        0.3713314        5.0000000E-02   -5.0000000E-02   -6.8566571E-02    1.8566571E-02        na               na       
 ippsy42              regul_ppsy    -1.000000       -0.8472235       -0.1527765        5.0000000E-02   -5.0000000E-02   -4.2361175E-02   -7.6388249E-03        na               na       
 ippsy43              regul_ppsy    -1.000000        -1.468266        0.4682660        5.0000000E-02   -5.0000000E-02   -7.3413302E-02    2.3413302E-02        na               na       
 ippsy44              regul_ppsy    -1.000000        -1.554952        0.5549520        5.0000000E-02   -5.0000000E-02   -7.7747602E-02    2.7747602E-02        na               na       
 ippsy45              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy46              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy47              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy48              regul_ppsy    -1.000000        -2.174627         1.174627        5.0000000E-02   -5.0000000E-02   -0.1087314        5.8731367E-02        na               na       
 ippsy49              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy50              regul_ppsy    -1.000000        -1.215309        0.2153092        5.0000000E-02   -5.0000000E-02   -6.0765461E-02    1.0765461E-02        na               na       
 ippsy51              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy52              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy53              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy54              regul_ppsy    -1.000000        -1.290508        0.2905076        5.0000000E-02   -5.0000000E-02   -6.4525379E-02    1.4525379E-02        na               na       
 ippsy55              regul_ppsy    -1.000000        -1.488708        0.4887081        5.0000000E-02   -5.0000000E-02   -7.4435407E-02    2.4435407E-02        na               na       
 ippsy56              regul_ppsy    -1.000000       -0.9007553       -9.9244747E-02    5.0000000E-02   -5.0000000E-02   -4.5037763E-02   -4.9622374E-03        na               na       
 ippsy57              regul_ppsy    -1.000000        -1.654900        0.6549000        5.0000000E-02   -5.0000000E-02   -8.2745001E-02    3.2745001E-02        na               na       
 ippsy58              regul_ppsy    -1.000000       -0.8284681       -0.1715319        5.0000000E-02   -5.0000000E-02   -4.1423404E-02   -8.5765964E-03        na               na       
 ippsy59              regul_ppsy    -1.000000       -0.7983298       -0.2016702        5.0000000E-02   -5.0000000E-02   -3.9916491E-02   -1.0083509E-02        na               na       
 ippsy60              regul_ppsy    -1.000000        -3.819359         2.819359        5.0000000E-02   -5.0000000E-02   -0.1909679        0.1409679            na               na       
 ippsy61              regul_ppsy    -1.000000        -1.271569        0.2715693        5.0000000E-02   -5.0000000E-02   -6.3578463E-02    1.3578463E-02        na               na       
 ippsy62              regul_ppsy    -1.000000        -1.025903        2.5903116E-02    5.0000000E-02   -5.0000000E-02   -5.1295156E-02    1.2951558E-03        na               na       
 ippsy63              regul_ppsy    -1.000000       -0.7073189       -0.2926811        5.0000000E-02   -5.0000000E-02   -3.5365946E-02   -1.4634054E-02        na               na       
 ippsy64              regul_ppsy    -1.000000       -0.6995306       -0.3004694        5.0000000E-02   -5.0000000E-02   -3.4976530E-02   -1.5023470E-02        na               na       
 ippsy65              regul_ppsy    -1.000000        -1.686159        0.6861588        5.0000000E-02   -5.0000000E-02   -8.4307940E-02    3.4307940E-02        na               na       
 ippsy66              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy67              regul_ppsy    -1.000000       -0.8358489       -0.1641511        5.0000000E-02   -5.0000000E-02   -4.1792444E-02   -8.2075565E-03        na               na       
 ippsy68              regul_ppsy    -1.000000       -0.9815575       -1.8442508E-02    5.0000000E-02   -5.0000000E-02   -4.9077875E-02   -9.2212539E-04        na               na       
 ippsy69              regul_ppsy    -1.000000       -0.8146126       -0.1853874        5.0000000E-02   -5.0000000E-02   -4.0730628E-02   -9.2693724E-03        na               na       
 ippsy70              regul_ppsy    -1.000000       -0.9140512       -8.5948836E-02    5.0000000E-02   -5.0000000E-02   -4.5702558E-02   -4.2974418E-03        na               na       
 ippsy71              regul_ppsy    -1.000000        -1.359737        0.3597368        5.0000000E-02   -5.0000000E-02   -6.7986840E-02    1.7986840E-02        na               na       
 ippsy72              regul_ppsy    -1.000000        -1.233224        0.2332240        5.0000000E-02   -5.0000000E-02   -6.1661198E-02    1.1661198E-02        na               na       
 ippsy73              regul_ppsy    -1.000000       -0.7955129       -0.2044871        5.0000000E-02   -5.0000000E-02   -3.9775646E-02   -1.0224354E-02        na               na       
 ippsy74              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy75              regul_ppsy    -1.000000       -0.9213312       -7.8668838E-02    5.0000000E-02   -5.0000000E-02   -4.6066558E-02   -3.9334419E-03        na               na       
 ippsy76              regul_ppsy    -1.000000        -1.280172        0.2801722        5.0000000E-02   -5.0000000E-02   -6.4008611E-02    1.4008611E-02        na               na       
 ippsy77              regul_ppsy    -1.000000        -1.331110        0.3311102        5.0000000E-02   -5.0000000E-02   -6.6555510E-02    1.6555510E-02        na               na       
 ippsy79              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy80              regul_ppsy    -1.000000        -1.047126        4.7126182E-02    5.0000000E-02   -5.0000000E-02   -5.2356309E-02    2.3563091E-03        na               na       
 ippsy81              regul_ppsy    -1.000000       -0.8275118       -0.1724882        5.0000000E-02   -5.0000000E-02   -4.1375589E-02   -8.6244107E-03        na               na       
 ippsy82              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy83              regul_ppsy    -1.000000        -1.068025        6.8025022E-02    5.0000000E-02   -5.0000000E-02   -5.3401251E-02    3.4012511E-03        na               na       
 ippsy84              regul_ppsy    -1.000000        -2.317039         1.317039        5.0000000E-02   -5.0000000E-02   -0.1158519        6.5851934E-02        na               na       
 ippsy85              regul_ppsy    -1.000000       -0.7004047       -0.2995953        5.0000000E-02   -5.0000000E-02   -3.5020234E-02   -1.4979766E-02        na               na       
 ippsy86              regul_ppsy    -1.000000        -1.413880        0.4138799        5.0000000E-02   -5.0000000E-02   -7.0693996E-02    2.0693996E-02        na               na       
 ippsy87              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy88              regul_ppsy    -1.000000        -1.004707        4.7070853E-03    5.0000000E-02   -5.0000000E-02   -5.0235354E-02    2.3535427E-04        na               na       
 ippsy89              regul_ppsy    -1.000000        -1.169328        0.1693281        5.0000000E-02   -5.0000000E-02   -5.8466405E-02    8.4664046E-03        na               na       
 ippsy91              regul_ppsy    -1.000000       -0.6989700       -0.3010300        5.0000000E-02   -5.0000000E-02   -3.4948500E-02   -1.5051500E-02        na               na       
 ippsy92              regul_ppsy    -1.000000        -1.553944        0.5539437        5.0000000E-02   -5.0000000E-02   -7.7697186E-02    2.7697186E-02        na               na       
 ippsy93              regul_ppsy    -1.000000        -1.545849        0.5458487        5.0000000E-02   -5.0000000E-02   -7.7292434E-02    2.7292434E-02        na               na       
