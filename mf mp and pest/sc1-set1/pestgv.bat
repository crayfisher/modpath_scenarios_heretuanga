::@echo off


:: Misc. cleanup
del HPM.hds > nul
del HPM_E1.hds > nul
del HPM_E2.hds > nul
del HPM_E3.hds > nul
del HPM_A.hds > nul
del HPM_M1.hds > nul
del HPM_M2.hds > nul
del head_smp.txt > nul
del hsim_e1.smp > nul
del hsim_e2.smp > nul
del hsim_e3.smp > nul
del hsim_a.smp > nul
del hsim_m1.smp > nul
del hsim_m2.smp > nul
del laydiff_bore_head_sample.txt > nul
del hsim_diff_e1.smp > nul
del hsim_diff_e2.smp > nul
del hsim_diff_e3.smp > nul
del hsim_diff_a.smp > nul
del hsim_diff_m1.smp > nul
del hsim_diff_m2.smp > nul
del drain_smp.txt > nul
del river_smp.txt > nul
del drain_smp.txt > nul
del slope_sim.smp >-nul

del HPM1._kx > nul
del HPM1._kz > nul
del HPM1._ss > nul
del HPM1._sy > nul
del HPM2._kx > nul
del HPM2._kz > nul
del HPM2._ss > nul
del HPM2._sy > nul

del head1_for_M1.ref > nul
del head1_for_M2.ref > nul
del head2_for_M1.ref > nul
del head2_for_M2.ref > nul

del HPM_A.wel > nul
del HPM_M1.wel > nul
del HPM_M2.wel > nul
                
del preprocessing.out> nul
del wel_gen.out > nul
del mf2005.out > nul
del mf2005_A.out > nul
del mf2005_M1.out > nul
del mf2005_M2.out > nul
del gen_init_heads.out > nul
del postprocessing.out > nul

:: Preprocessing
fac2real < fac2r_kx_L1.in > preprocessing.out   
fac2real < fac2r_kx_L2.in >> preprocessing.out  
fac2real < fac2r_kz_L1.in >> preprocessing.out  
fac2real < fac2r_kz_L2.in >> preprocessing.out  
fac2real < fac2r_Ss_L1.in >> preprocessing.out  
fac2real < fac2r_Ss_L2.in >> preprocessing.out  
fac2real < fac2r_Sy_L1.in >> preprocessing.out  
fac2real < fac2r_Sy_L2.in >> preprocessing.out  

Rscript  create_well_file2.R
Rscript  riv_mult_proc.R
:: Simulation
mf2005 HPM.nam    > mf2005.out
mf2005 HPM_80.nam >> mf2005.out
mf2005 HPM_E1.nam >> mf2005.out
mf2005 HPM_E2.nam >> mf2005.out
mf2005 HPM_E3.nam >> mf2005.out

mf2005 HPM_A.nam > mf2005_A.out
getmularr < getmularr_for_m1m2.in > gen_init_heads.out
mf2005 HPM_M1.nam > mf2005_M1.out
mf2005 HPM_M2.nam > mf2005_M2.out


:: Postprocessing
:: steady state
mod2obs < head_mod2obs.in > postprocessing.out
mod2obs < laydiff_mod2obs.in >> postprocessing.out
laydiff < laydiff.in >> postprocessing.out
bud2smp < river_bud2smp.in >> postprocessing.out
bud2smp < drain_bud2smp.in >> postprocessing.out
copy drain_smp.dummy drain_smp.txt

obs2obs drain_obs2obs.in  drain_obs2obs.out

:: events
mod2obs < head_mod2obs_e1.in >> postprocessing.out
mod2obs < head_mod2obs_e2.in >> postprocessing.out
mod2obs < head_mod2obs_e3.in >> postprocessing.out

smpdiff < smpdiff_e1.in >> postprocessing.out
smpdiff < smpdiff_e2.in >> postprocessing.out
smpdiff < smpdiff_e3.in >> postprocessing.out

:: annual and monthly
mod2obs < head_mod2obs_a.in >> postprocessing.out
mod2obs < head_mod2obs_m1.in >> postprocessing.out
mod2obs < head_mod2obs_m2.in >> postprocessing.out

smpdiff < smpdiff_a.in >> postprocessing.out
smpdiff < smpdiff_m1.in >> postprocessing.out
smpdiff < smpdiff_m2.in >> postprocessing.out

Rscript --default-packages=methods read_bud2smp.R

Rscript pest_trend3.R
