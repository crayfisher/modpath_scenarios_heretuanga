this file described how to run modflow and modpaht through scenarios

sc1_set1 folder contains all files needed for a stochastic run

sc - scenario
set - set of realisations (realisatons were divided into 2 sets 53 and 54 elements)

folders are created to divede run into several chunks that can be run on one pc with multiple cores (optimied for 8 cores)

to run:
copy sc1_set1 folder 7 times and rename to sc[1-4]-set[1-2] names need to exact as python scrip uses foled names to check which scenario and set is used
then use python scrip run_realisations.py
this will:
check which scenario and realisations set is used
will import modflow and create modpath model (using correct scenario for particle locations from my_)
particle locations
then will loop through realisations, 
for each realisations runnnin parrep to create new pest
run pest with noptmax0
pest run includes modifed r scrip that creates a well file for appropriate scenario
run modflow
run modpath
copy timeseries files, and modflow and modpath listing files
post processing is done separaelly

to modify a scenario, change my_data.feather file (it contains well locations for modpath, can be generated with R from a data frame)
